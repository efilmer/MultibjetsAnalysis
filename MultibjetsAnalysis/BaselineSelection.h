#ifndef MultibjetsAnalysis_BaselineSelection_H
#define MultibjetsAnalysis_BaselineSelection_H

#include <MultibjetsAnalysis/MuonHelper.h>
#include <MultibjetsAnalysis/ElectronHelper.h>
#include <MultibjetsAnalysis/JetHelper.h>
#include <xAODMissingET/MissingET.h>
#include "xAODMissingET/MissingETContainer.h"

class BaselineSelection {

 public:
  BaselineSelection();
  ~BaselineSelection();

  static bool keep(
          bool doBaselineSelection,
          const xAOD::MissingETContainer* met,
		      const xAOD::JetContainer* v_jets,
		      const xAOD::MuonContainer* v_muons,
		      const xAOD::ElectronContainer*  v_electrons,
          const bool doQCD,
		      int flag);



};

#endif
