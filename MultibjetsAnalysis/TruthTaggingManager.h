#ifndef TRUTHTAGGINGMANAGER_H
#define TRUTHTAGGINGMANAGER_H


#include "BTaggingTruthTagging/BTaggingTruthTaggingTool.h"
#include <vector>
#include <map>
#include <iostream>

#include "xAODJet/JetContainer.h"

struct TTres{
  std::map<std::string,std::vector<double> > ttwei_ex;
  std::map<std::string,std::vector<double> > ttwei_in;
  std::vector<std::vector<bool> > perm_ex;
  std::vector<std::vector<bool> > perm_in;
  std::vector<std::vector<int> > quant_ex; //for each tag mult, vector of int: quantile of each jet
  std::vector<std::vector<int> > quant_in;
};

class TruthTaggingManager{

public:
  //____________________________________________________________________________
  //
  TruthTaggingManager();
  virtual ~TruthTaggingManager();

  //____________________________________________________________________________
  //
  void initialize( const int ShowerType );
  void compute_wei(const xAOD::JetContainer*& jets, bool do_syst = false);
  void print_all();
  TTres m_TTres;
  std::vector<std::string> affectingSystematics(){return m_btt->affectingSystematics();};

private:
  BTaggingTruthTaggingTool * m_btt;
  void clear_ttres();

};

#endif
