#ifndef MultibjetsAnalysis_TreeMaker_h
#define MultibjetsAnalysis_TreeMaker_h

#include <TTree.h>
#include <TFile.h>

#include "PATInterfaces/SystematicSet.h"
#include "SUSYTools/ISUSYObjDef_xAODTool.h"

#include "MultibjetsAnalysis/MuonHelper.h"
#include "MultibjetsAnalysis/ElectronHelper.h"
#include "MultibjetsAnalysis/JetHelper.h"
#include "MultibjetsAnalysis/TruthParticleHelper.h"
#include <xAODMissingET/MissingET.h>

//Top/ttbar pT reweighting
#include "MultibjetsAnalysis/TopTtbarPtReweighting.h"

// matrix method
#include "xAODBTaggingEfficiency/BTaggingEfficiencyTool.h"
#include "MultibjetsAnalysis/MatrixMethod_bjets.h"

// matrix method
#include <map>
#include <string>

// TRF
#include "MultibjetsAnalysis/TruthTaggingManager.h"

class TRandom3;

class TreeMaker
{

 public:

  TreeMaker(TFile *file, bool do_truth, bool do_rc_jets, bool do_vrc_jets, bool do_ak10_jets, bool do_TRF,
	    std::vector<std::string> btagging_systematics,
	    int ttbarHFCorrection, bool do_bTagEff, bool doMM, bool do_syst,
	    std::vector<ST::SystInfo> sys_list, const std::map< std::string, bool > &v_trigger);
  virtual ~TreeMaker();

  void Fill_obj(unsigned long long event_number,
		long int run_number, long int random_run_number,
		float average_interactions_per_crossing,
		float actual_interactions_per_crossing,
		float primary_vertex_z,
    int n_primary_vertices,
		int process,
		int ttbar_class,
		int ttbar_class_ext,
		int ttbar_class_prompt,
        std::map < std::string, float > &ttbb_weight_map,
		int mask_topdecay,
		int mask_antitop_decay,
		const xAOD::MuonContainer* v_muons,
		const xAOD::ElectronContainer* v_electrons,
		const xAOD::JetContainer* v_jets,
		const xAOD::MissingETContainer* v_metcst,
		const xAOD::MissingETContainer* v_mettst,
		const std::map< std::string, bool > &v_trigger,
		std::map < const std::string, Double_t > weight_map,
		bool do_TRF,
		const TTres* ttres,
		const xAOD::EventInfo* info,
    xAOD::TEvent*& event);

  void Fill_truth(const xAOD::TruthParticleContainer* v_truth, TLorentzVector v_met_truth/*, Float_t met_truth_filter*/);

  void Fill_rc_jets(xAOD::TStore*& m_store, std::string sys_name, bool is_nominal, bool affects_jets, int maxjets_mjsum);
  void Fill_vrc_jets(xAOD::TStore*& m_store, std::string sys_name, bool is_nominal, bool affects_jets, int maxjets_mjsum);
  void Fill_ak10_jets(const xAOD::JetContainer* v_ak10_jets);

  // jets b-tagging efficiency for the matrix method
  void Fill_bTag_efficiencies(const xAOD::JetContainer* v_jets);
  // matrix method weights
  void Fill_matrix_method(const xAOD::JetContainer* v_jets);

  void Fill(std::string sys_name);
  void Write();

 protected:

  void Reset();

  std::map<std::string, TTree*>   m_tree_map; //!

  unsigned long long m_event_number; //!
  Long_t m_run_number;   //!
  Long_t m_random_run_number;   //!
  Int_t m_lumiblock_number; //!
  Float_t m_average_interactions_per_crossing; //!
  Float_t m_actual_interactions_per_crossing; //!
  Int_t m_n_vtx;
  Float_t m_primary_vertex_z; //!
  Int_t m_process;      //!
  Float_t m_gen_filt_ht; //!
  Float_t m_gen_filt_met; //!
  Int_t m_ttbar_class;  //!
  Int_t m_ttbar_class_ext;  //!
  Int_t m_ttbar_class_prompt;  //!
  Int_t m_top_decay_type;//!
  Int_t m_antitop_decay_type;//!
  Int_t m_ttbar_hf_filter_flag;//!
  Int_t m_truth_jets_n;//!

  Int_t m_muons_n;                       //!
  std::vector<Float_t> m_muons_pt;       //!
  std::vector<Float_t> m_muons_eta;      //!
  std::vector<Float_t> m_muons_phi;      //!
  std::vector<Float_t> m_muons_e;        //!
  std::vector<Int_t>   m_muons_passOR;   //!
  std::vector<Int_t>   m_muons_isSignal; //!
  std::vector<Int_t>   m_muons_isCosmic; //!
  std::vector<Int_t>   m_muons_isBad;    //!
  std::vector<Float_t> m_muons_ptvarcone20; //!
  std::vector<Float_t> m_muons_ptvarcone30; //!
  std::vector<Float_t> m_muons_topoetcone20; //!
  std::vector<Double_t> m_muons_d0sig;   //!
  std::vector<Double_t> m_muons_z0;      //!
  std::vector<Int_t> m_muons_charge;      //!
  std::vector<Int_t> m_muons_isTriggerMatch; //!

  Int_t m_electrons_n;                       //!
  std::vector<Float_t> m_electrons_pt;       //!
  std::vector<Float_t> m_electrons_eta;      //!
  std::vector<Float_t> m_electrons_phi;      //!
  std::vector<Float_t> m_electrons_e;        //!
  std::vector<Int_t> m_electrons_passOR;   //!
  std::vector<Int_t> m_electrons_isSignal;   //!
  std::vector<Int_t> m_electrons_passId;     //!
  std::vector<Float_t> m_electrons_ptvarcone20; //!
  std::vector<Float_t> m_electrons_ptvarcone30; //!
  std::vector<Float_t> m_electrons_topoetcone20; //!
  std::vector<Double_t> m_electrons_d0sig;   //!
  std::vector<Double_t> m_electrons_z0;      //!
  std::vector<Int_t> m_electrons_charge;     //!
  std::vector<Int_t> m_electrons_isTriggerMatch; //!

  Int_t m_jets_n;                     //!
  std::vector<Float_t> m_jets_pt;     //!
  std::vector<Float_t> m_jets_eta;    //!
  std::vector<Float_t> m_jets_phi;    //!
  std::vector<Float_t> m_jets_e;      //!
  std::vector<Int_t> m_jets_passOR; //!
  std::vector<Int_t> m_jets_isBad;  //!
  std::vector<Int_t> m_jets_isSignal;  //!
  std::vector<Float_t> m_jets_jvt;  //!
  std::vector<Int_t> m_jets_nTracks;//!
  std::vector<Int_t> m_jets_truthLabel;    //!
  std::vector<Double_t> m_jets_btag_weight;   //!
  std::vector<Int_t>  m_jets_isb_60;  //!
  std::vector<Int_t>  m_jets_isb_70;  //!
  std::vector<Int_t>  m_jets_isb_77;  //!
  std::vector<Int_t>  m_jets_isb_85;  //!
  std::vector<Double_t> m_jets_btagEff_weight;   //!

  //===== VRC ===============
  Int_t m_vrc_r10rho250_jets_n;                  //!
  std::vector<Float_t> m_vrc_r10rho250_jets_pt;  //!
  std::vector<Float_t> m_vrc_r10rho250_jets_eta; //!
  std::vector<Float_t> m_vrc_r10rho250_jets_phi; //!
  std::vector<Float_t> m_vrc_r10rho250_jets_e;   //!
  std::vector<Float_t> m_vrc_r10rho250_jets_m;   //!
  std::vector<Float_t> m_vrc_r10rho250_jets_Reff;   //!
  std::vector<Int_t> m_vrc_r10rho250_jets_nconst;   //!

  Int_t m_vrc_r10rho350_jets_n;                  //!
  std::vector<Float_t> m_vrc_r10rho350_jets_pt;  //!
  std::vector<Float_t> m_vrc_r10rho350_jets_eta; //!
  std::vector<Float_t> m_vrc_r10rho350_jets_phi; //!
  std::vector<Float_t> m_vrc_r10rho350_jets_e;   //!
  std::vector<Float_t> m_vrc_r10rho350_jets_m;   //!
  std::vector<Float_t> m_vrc_r10rho350_jets_Reff;   //!
  std::vector<Int_t> m_vrc_r10rho350_jets_nconst;   //!

  Int_t m_vrc_r10rho300_jets_n;                  //!
  std::vector<Float_t> m_vrc_r10rho300_jets_pt;  //!
  std::vector<Float_t> m_vrc_r10rho300_jets_eta; //!
  std::vector<Float_t> m_vrc_r10rho300_jets_phi; //!
  std::vector<Float_t> m_vrc_r10rho300_jets_e;   //!
  std::vector<Float_t> m_vrc_r10rho300_jets_m;   //!
  std::vector<Float_t> m_vrc_r10rho300_jets_Reff;   //!
  std::vector<Int_t> m_vrc_r10rho300_jets_nconst;   //!

  Int_t m_vrc_r12rho300_jets_n;                  //!
  std::vector<Float_t> m_vrc_r12rho300_jets_pt;  //!
  std::vector<Float_t> m_vrc_r12rho300_jets_eta; //!
  std::vector<Float_t> m_vrc_r12rho300_jets_phi; //!
  std::vector<Float_t> m_vrc_r12rho300_jets_e;   //!
  std::vector<Float_t> m_vrc_r12rho300_jets_m;   //!
  std::vector<Float_t> m_vrc_r12rho300_jets_Reff;   //!
  std::vector<Int_t> m_vrc_r12rho300_jets_nconst;   //!

  //========= Fixed-R RC ==============
  Int_t m_rc_R10PT05_jets_n;                  //!
  std::vector<Float_t> m_rc_R10PT05_jets_pt;  //!
  std::vector<Float_t> m_rc_R10PT05_jets_eta; //!
  std::vector<Float_t> m_rc_R10PT05_jets_phi; //!
  std::vector<Float_t> m_rc_R10PT05_jets_e;   //!
  std::vector<Float_t> m_rc_R10PT05_jets_m;   //!
  std::vector<Int_t> m_rc_R10PT05_jets_nconst;   //!

  //========= Large-R jets ==============
  Int_t m_ak10_jets_n;                  //!
  std::vector<Float_t> m_ak10_jets_pt;  //!
  std::vector<Float_t> m_ak10_jets_eta; //!
  std::vector<Float_t> m_ak10_jets_phi; //!
  std::vector<Float_t> m_ak10_jets_e;   //!
  std::vector<Float_t> m_ak10_jets_m;   //!
  std::vector<Float_t> m_ak10_jets_SPLIT12;   //!
  std::vector<Float_t> m_ak10_jets_SPLIT23;   //!
  std::vector<Float_t> m_ak10_jets_Tau21;   //!
  std::vector<Float_t> m_ak10_jets_Tau32;   //!
  std::vector<Float_t> m_ak10_jets_isTopLoose;   //!
  std::vector<Float_t> m_ak10_jets_isTopTight;   //!
  std::vector<Float_t> m_ak10_jets_isTopSmoothLoose;   //!
  std::vector<Float_t> m_ak10_jets_isTopSmoothTight;   //!

  //Int_t m_tst_clean;     //!

  Float_t m_metcst;     //!
  Float_t m_metcst_phi; //!
  Float_t m_mettst;     //!
  Float_t m_mettst_phi; //!
  Float_t m_metsoft;     //!
  Float_t m_metsoft_phi; //!

  Float_t m_meff;           //!
  Float_t m_ht;             //!

  Float_t m_met_sig;        //!
  Float_t m_mt;             //!
  Float_t m_mt_min_bmet;    //!
  Float_t m_mt_min_bmetW;   //!

  std::map < const std::string, Double_t > m_weight_nom_map; //!
  std::map < const std::string, Double_t > m_weight_sys_map; //!
  std::map < const std::string, Double_t > m_weight_topPt_systs;//!
  std::map < const std::string, Double_t > m_weight_ttbb;//!

  // truth
  Int_t m_mc_n;                         //!
  std::vector < float > m_mc_pt;        //!
  std::vector < float > m_mc_eta;       //!
  std::vector < float > m_mc_phi;       //!
  std::vector < float > m_mc_m;       //!
  std::vector < float > m_mc_status;    //!
  std::vector < float > m_mc_pdgId;     //!
  std::vector < std::vector < int > > m_mc_children_index;  //!
  Float_t m_met_truth;        //!
  Float_t m_met_truth_phi;    //!
  //Float_t m_met_truth_filter; //!
  Float_t m_mtt;              //!

  // trigger
  std::map < std::string, int > m_trigger; //!
  std::map < std::string, std::vector < int > > m_el_trigger; //!
  std::map < std::string, std::vector < int > > m_mu_trigger; //!

  // chiara: TRF vectors
  std::vector<bool> m_perm_2bex;
  std::vector<bool> m_perm_3bex;
  std::vector<bool> m_perm_4bin;
  std::map < std::string, double > m_ttwei_2bex; //!
  std::map < std::string, double > m_ttwei_3bex; //!
  std::map < std::string, double > m_ttwei_4bin; //!

  // b-tagging efficiencies for the matrix method
  std::vector < float > m_jets_bTagEff_85; //!
  std::vector < float > m_jets_bTagEff_85_down; //!
  std::vector < float > m_jets_bTagEff_85_up; //!

  BTaggingEfficiencyTool * m_bTag_tool;

  // Matrix method weights
  MatrixMethod_bjets * m_MM; //!

  // weights for jets with pt 30 > GeV
  Double_t m_MM_weight_85_30;        //!   nominal
  Double_t m_MM_weight_85_30_bd;     //!   efficiency for B down
  Double_t m_MM_weight_85_30_cd;     //!   efficiency for C down
  Double_t m_MM_weight_85_30_ld;     //!   efficiency for Light down
  Double_t m_MM_weight_85_30_bu;     //!   efficiency for B up
  Double_t m_MM_weight_85_30_cu;     //!   efficiency for C up
  Double_t m_MM_weight_85_30_lu;     //!   efficiency for Light up
  Double_t m_MM_weight_85_30_MCstat; //!   MC stat uncertainty
  Double_t m_MM_weight_85_30_data;   //!   fake rate from data for systematics

  // weights for jets with pt 30 > GeV
  Double_t m_MM_weight_85_50;        //!   nominal
  Double_t m_MM_weight_85_50_bd;     //!   efficiency for B down
  Double_t m_MM_weight_85_50_cd;     //!   efficiency for C down
  Double_t m_MM_weight_85_50_ld;     //!   efficiency for Light down
  Double_t m_MM_weight_85_50_bu;     //!   efficiency for B up
  Double_t m_MM_weight_85_50_cu;     //!   efficiency for C up
  Double_t m_MM_weight_85_50_lu;     //!   efficiency for Light up
  Double_t m_MM_weight_85_50_MCstat; //!   MC stat uncertainty
  Double_t m_MM_weight_85_50_data;   //!   fake rate from data for systematics

  // weights for jets with pt 30 > GeV
  Double_t m_MM_weight_85_70;        //!   nominal
  Double_t m_MM_weight_85_70_bd;     //!   efficiency for B down
  Double_t m_MM_weight_85_70_cd;     //!   efficiency for C down
  Double_t m_MM_weight_85_70_ld;     //!   efficiency for Light down
  Double_t m_MM_weight_85_70_bu;     //!   efficiency for B up
  Double_t m_MM_weight_85_70_cu;     //!   efficiency for C up
  Double_t m_MM_weight_85_70_lu;     //!   efficiency for Light up
  Double_t m_MM_weight_85_70_MCstat; //!   MC stat uncertainty
  Double_t m_MM_weight_85_70_data;   //!   fake rate from data for systematics

  // weights for jets with pt 30 > GeV
  Double_t m_MM_weight_85_90;        //!   nominal
  Double_t m_MM_weight_85_90_bd;     //!   efficiency for B down
  Double_t m_MM_weight_85_90_cd;     //!   efficiency for C down
  Double_t m_MM_weight_85_90_ld;     //!   efficiency for Light down
  Double_t m_MM_weight_85_90_bu;     //!   efficiency for B up
  Double_t m_MM_weight_85_90_cu;     //!   efficiency for C up
  Double_t m_MM_weight_85_90_lu;     //!   efficiency for Light up
  Double_t m_MM_weight_85_90_MCstat; //!   MC stat uncertainty
  Double_t m_MM_weight_85_90_data;   //!   fake rate from data for systematics

  TRandom3* m_rdm;
};

#endif //MultibjetsAnalysis_TreeMaker_h
