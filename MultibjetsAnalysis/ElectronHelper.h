#ifndef MultibjetsAnalysis_ElectronHelper_h
#define MultibjetsAnalysis_ElectronHelper_h

#include <utility>

// xAOD
#include "xAODEgamma/Electron.h"
#include "xAODEgamma/ElectronContainer.h"

#include "xAODEventInfo/EventInfo.h"

#include "xAODCore/ShallowCopy.h"
#include "xAODRootAccess/TStore.h"

#include <memory>

#include <map>
#include <string>

namespace ST{
class SUSYObjDef_xAOD;
}

class ElectronHelper
{

 public:

  ElectronHelper(ST::SUSYObjDef_xAOD*& susy_tools, xAOD::TEvent*& event, xAOD::TStore*& store);

  // Apply kinematic and quality cuts
  void DecorateBaselineElectrons(xAOD::ElectronContainer*& electrons, const xAOD::EventInfo *info, std::string sys_name = "");

  const xAOD::ElectronContainer*  GetBaselineElectrons(bool is_nominal, bool affects_electrons, std::string sys_name, xAOD::TStore*& m_store) {
     const xAOD::ElectronContainer* selected_electrons(nullptr);
     std::string name = "SelectedElectrons";
     if(!is_nominal && affects_electrons) name += sys_name;
     if(!m_store->retrieve( selected_electrons, name ).isSuccess()) std::cout << "major problem ! " << std::endl;
     return selected_electrons;
  };

  const xAOD::ElectronContainer*  GetSignalElectrons(bool is_nominal, bool affects_electrons, std::string sys_name, xAOD::TStore*& m_store) {
     const xAOD::ElectronContainer* signal_electrons(nullptr);
     std::string name = "SignalElectrons";
     if(!is_nominal && affects_electrons) name += sys_name;
     if(!m_store->retrieve( signal_electrons, name ).isSuccess()) std::cout << "major problem ! " << std::endl;
     return signal_electrons;
  };

  // set the vector of triggers
  inline void SetTriggerVector( const std::map < std::string, bool > &vec ) { m_triggers = vec; }

 protected:

   // View container of signal electrons
  xAOD::TEvent*& m_event;  //!
  xAOD::TStore*& m_store;  //!

  // SUSYTools
  ST::SUSYObjDef_xAOD*& m_susy_tools ; //!

  //vector of strings for the triggers to inspect
  std::map < std::string, bool > m_triggers;

};

#endif
