#ifndef MultibjetsAnalysis_MuonHelper_h
#define MultibjetsAnalysis_MuonHelper_h

#include <utility>

// xAOD
#include "xAODEventInfo/EventInfo.h"
#include "xAODMuon/Muon.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"

#include "xAODCore/ShallowCopy.h"
#include "xAODRootAccess/TStore.h"

#include <memory>

#include <map>
#include <string>

namespace ST{
class SUSYObjDef_xAOD;
}

class MuonHelper
{

 public:

  MuonHelper(ST::SUSYObjDef_xAOD*& susy_tools, xAOD::TEvent*& event, xAOD::TStore*& store);

  // Apply kinematic and quality cuts
  void DecorateBaselineMuons(xAOD::MuonContainer*& muons, const xAOD::EventInfo *info, std::string sys_name="");

  // return number of bad muons before OR
  UInt_t GetNbBadMuons()   {return m_nb_bad_muons;};

  // return number of cosmic muons after OR
  UInt_t GetNbCosmicMuons()   {return m_nb_cosmic_muons;};

  const xAOD::MuonContainer* GetBaselineMuons(bool is_nominal, bool affects_muons, std::string sys_name, xAOD::TStore*& m_store)   {
     const xAOD::MuonContainer* selected_muons(nullptr);
     std::string name = "SelectedMuons";
     if(!is_nominal && affects_muons) name += sys_name;
     if(!m_store->retrieve( selected_muons, name ).isSuccess()) std::cout << "major problem ! " << std::endl;
     return selected_muons;
  };

  const xAOD::MuonContainer* GetSignalMuons(bool is_nominal, bool affects_muons, std::string sys_name, xAOD::TStore*& m_store)   {
     const xAOD::MuonContainer* signal_muons(nullptr);
     std::string name = "SignalMuons";
     if(!is_nominal && affects_muons) name += sys_name;
     if(!m_store->retrieve( signal_muons, name ).isSuccess()) std::cout << "major problem ! " << std::endl;
     return signal_muons;
  };


  // set the vector of triggers
  inline void SetTriggerVector( const std::map < std::string, bool > &vec ) { m_triggers = vec; }

 protected:

  xAOD::TEvent*& m_event;  //!
  xAOD::TStore*& m_store; //!

  // SUSYTools
  ST::SUSYObjDef_xAOD*& m_susy_tools ; //!

  // number of bad and cosmic muons
  UInt_t m_nb_bad_muons;
  UInt_t m_nb_cosmic_muons;

  //vector of strings for the triggers to inspect
  std::map < std::string, bool > m_triggers;

};

#endif
