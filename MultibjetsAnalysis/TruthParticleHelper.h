#ifndef MultibjetsAnalysis_TruthParticleHelper_h
#define MultibjetsAnalysis_TruthParticleHelper_h

#include <utility>
#include <set>

// xAOD
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthParticleAuxContainer.h"
#include "xAODJet/Jet.h"
#include "xAODJet/JetContainer.h"

#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"

#include "xAODCore/ShallowCopy.h"


class TruthParticleHelper
{

public:

    //
    // Standard C++ functions
    //
    TruthParticleHelper(xAOD::TEvent*& event, xAOD::TStore*& store);
    TruthParticleHelper( const TruthParticleHelper &t );
    virtual ~TruthParticleHelper();

    // Retreive the truth particle container
    void RetrieveTruthParticles();
    // Fills the vector of truth particles with the desired characteristics
    void RetrieveSignalTruthParticles();

    xAOD::TruthParticleContainer GetShallowCopy() {return *m_truthparticle_copy;};
    // make deep copy of the truth particles container in the output xAOD
    void MakeDeepCopy();
    const xAOD::TruthParticleContainer* GetSignalTruthParticles() { return m_truthparticle_vector; };
    // calculates the mtt mass for the ttbar mtt sliced samples
    static Float_t mtt(std::vector<float> mc_pt,
                       std::vector<float> mc_eta,
                       std::vector<float> mc_phi,
                       std::vector<float> mc_pdgId,
                       std::vector<float> mc_status);

    // calculate the met for the ttbar sample with met filter
    Float_t truth_met_filter();

    //utilities to compute the truth HT variable
    bool isLeptonForHT(const xAOD::TruthParticle * particle);
    bool isJetForHT(const xAOD::Jet * jet);

    // calculate the ht for the ttbar sample with met filter
    Float_t truth_ht_filter();

    int GetTopDecayMode( const bool ) const;

private:
    void GetLastChildren( const xAOD::TruthParticle *truthpart, std::set < int > &indices );
    void ReturnAllDescendantIndices( const xAOD::TruthParticle *truthpart, std::set < int > &indices );
    void ReturnAllAncestorsIndices( const xAOD::TruthParticle *truthpart, std::set < int > &indices );
    bool IsInterestingPDGId( const unsigned int absPdgId );

protected:
    xAOD::TEvent *m_event; //!
    xAOD::TStore *m_store; //!

    // shallow copies
    xAOD::TruthParticleContainer* m_truthparticle_copy;
    xAOD::ShallowAuxContainer*    m_truthparticle_copy_aux;

    // vector of truth particles
    const xAOD::TruthParticleContainer* m_truthparticle_vector;
};

#endif //MultibjetsAnalysis_TruthParticleHelper_h
