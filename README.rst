Checking Out
============

To checkout, grab all necessary packages::

  rcSetup Base,2.4.28
  git clone ssh://git@gitlab.cern.ch:7999/htx/MultibjetsAnalysis.git
  #to get a specific tag, you can do
  #cd MultibjetsAnalysis && git checkout MultibjetsAnalysis-XX-YY-ZZ && cd -
  rc checkout MultibjetsAnalysis/packages.txt
  git clone ssh://git@gitlab.cern.ch:7999/htx/IFAEReweightingTools.git
  cd IFAEReweightingTools && git checkout IFAEReweightingTools-00-01-06-08 && cd -
  rc checkout SUSYTools/doc/packages.txt

Patches
--------
To apply impact parameter significance cuts to baseline leptons, the following patch must be applied in SUSYTools::

 cd SUSYTools && patch -p0 -i ../MultibjetsAnalysis/SUSYTools.patch && cd -


Compiling
---------

.. code::

  rc clean
  rc find_packages
  rc compile


Running on the Grid
===================

You need to go back to the main folder (the one with ``rcSetup.sh``).

.. code::

  python Run.py --submitDir ttbar_13TeV --inputDS mc14_13TeV.110401.PowhegPythia_P2012_ttbar_nonallhad.merge.AOD.e2928_a262_a265_r5853/ --doPileup 0 --driver grid

  python Run.py --submitDir ttbar --inputDS mc14_8TeV.117050.PowhegPythia_P2011C_ttbar.merge.AOD.e1727_s1933_s1911_r5778_r5625/ --driver grid

  python Run.py --submitDir data --inputDS data12_8TeV.periodB.physics_JetTauEtmiss.PhysCont.AOD.repro16_v33/ --isData 1 --driver grid

  ./MultibjetsAnalysis/scripts/submit.py MultibjetsAnalysis/scripts/processes/Gtt.list

  ./MultibjetsAnalysis/scripts/submit.py MultibjetsAnalysis/scripts/processes/ttbar.list

  ./MultibjetsAnalysis/scripts/submit.py MultibjetsAnalysis/scripts/processes/ttbar.list MultibjetsAnalysis/scripts/processes/topEW.list

  # do a dry run to see what commands would be executed
  ./MultibjetsAnalysis/scripts/submit.py -n MultibjetsAnalysis/scripts/processes/*.list


Package Development
===================

Tagging
-------

The naming convention for tags is quite simple. Provide a tag of the format::

  X.Y.Z-R

where ``X.Y.Z`` correspond to the ``AnalysisBase`` release we are working on. For example, if you run ``rcSetup SUSY,2.3.41``, then ``X.Y.Z`` will be ``2.3.41``. In the rare case where you have to check out a version of the release which was an emergency patch, such as ``rcSetup SUSY,2.3.38a``, then it is acceptable to have ``X.Y.Z`` be ``2.3.38a`` as it will be clear and understood. The ``R`` is the MultibjetsAnalysis revision number. This will generally be incremented up from the previous one.

To find previous tags, just run::

  git tag -l MultibjetsAnalysis-X.Y.Z-*

and figure out what the previous revision was for a given release which we based the tag on. **Please use zero-indexing with the MBJ revision number.**

To actually tag a piece of code, I prefer that everyone uses annotated tagging. See `this StackOverflow answer <http://stackoverflow.com/a/11514139/1532974>`_ for why. At the time of writing this readme, I plan to use the new tagging names so we start at revision ``0`` on the ``2.3.41`` release, so::

  git tag -a MultibjetsAnalysis-2.3.41-0

which will bring up an editor for me to type a message in. This is generally the message of a ChangeLog I would use to describe all the major changes since the last tag. A ChangeLog is a hassle to maintain and there's rarely a reason to do so when you can provide messages in the tag itself. If you are tagging with a quick change, you can use the convenience command::

  git tag -a MultibjetsAnalysis-2.3.41-0 -m "A really brief message describing the tag.
  >
  > I can also press <Enter> to continue on multiple lines."

and we're good to go. Don't forget to upload the tag to the remote::

  git push --tags

Dataset Replicas on MWT2
========================

I ran the following command (for example)::

  for dataset in `rucio list-dids --short user.mleblanc:user.mleblanc.*.8june_tree.root | sort`; do echo $dataset ; rucio add-rule $dataset 1 MWT2_UC_LOCALGROUPDISK; done

which should add the rule to all datasets that match the above pattern to replicate on ``MWT2_UC_LOCALGROUPDISK``. This does not seem to work correctly.
