#include <MultibjetsAnalysis/JetHelper.h>

#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "xAODBase/IParticleHelpers.h"

#include "JetSelectorTools/JetCleaningTool.h"

// jet reclustering
#include <fastjet/PseudoJet.hh>
#include <fastjet/ClusterSequence.hh>

// jet trimming
#include <fastjet/tools/Filter.hh>
#include <JetEDM/JetConstituentFiller.h>

#include "TH2F.h"

//______________________________________________________________________________
//
JetHelper::JetHelper(ST::SUSYObjDef_xAOD*& susy_tools, xAOD::TEvent*& event, xAOD::TStore*& store) :
m_event(event),
m_store(store),
m_susy_tools(susy_tools),
m_fj_copy(0),
m_fj_copy_aux(0),
m_clus_copy(0),
m_clus_copy_aux(0),
m_fat_jets(0),
m_nb_bad_jets(0){
}

//______________________________________________________________________________
//
JetHelper::~JetHelper(){
}

//______________________________________________________________________________
// Apply kinematic and quality cuts
void JetHelper::DecorateBaselineJets(xAOD::JetContainer*& jets, std::string sys_name)
{
  std::string name = "BaselineJets"+sys_name;
  ConstDataVector<xAOD::JetContainer> * baseline_jets   =  new ConstDataVector<xAOD::JetContainer>(SG::VIEW_ELEMENTS);
  if(!m_store->record(baseline_jets,   name).isSuccess()) std::cout << "Could not record selected baseline jets" << std::endl;

  std::string nameSig = "SignalJets"+sys_name;
  ConstDataVector<xAOD::JetContainer> * signal_jets     =  new ConstDataVector<xAOD::JetContainer>(SG::VIEW_ELEMENTS);
  if(!m_store->record(signal_jets, nameSig).isSuccess()) std::cout << "Could not record selected signal jets" << std::endl;

  std::string nameB = "SignalBJets"+sys_name;
  ConstDataVector<xAOD::JetContainer> * selected_b_jets =  new ConstDataVector<xAOD::JetContainer>(SG::VIEW_ELEMENTS);
  if(!m_store->record(selected_b_jets, nameB).isSuccess()) std::cout << "Could not record selected b-jets" << std::endl;

  static SG::AuxElement::ConstAccessor< char > bad("bad");
  static SG::AuxElement::ConstAccessor< char > baseline("baseline");
  static SG::AuxElement::ConstAccessor< char > signal("signal");
  static SG::AuxElement::ConstAccessor< char > passOR("passOR");

  m_nb_bad_jets = 0;

  for(const auto &jet : *jets) {

    //decorate b-jets
    m_susy_tools->IsBJet( *jet );

    //define baseline jets
    if(!baseline.isAvailable(*jet) || baseline(*jet)){
      baseline_jets->push_back(jet);
    }

    //define signal jets
    if(baseline(*jet) && signal(*jet) && passOR(*jet) ) {
      signal_jets->push_back(jet);
      // kinematic cuts applied to b-jets as needed the b-tagging SFs
      if(fabs(jet->eta())<2.5){
        selected_b_jets->push_back(jet);
      }
    }

    //-bad jets identification
    //based on: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/HowToCleanJets2015
    if( passOR(*jet) ) {
      if(!bad.isAvailable(*jet) || bad(*jet) == 1){//the isBad decoration already takes into account JVT and pt/eta
        m_nb_bad_jets++;
      }
    }
  }//loop over jets

  // sort in pT
  std::sort (baseline_jets->begin(), baseline_jets->end(), HelperFunctions::pt_sort());
  std::sort (selected_b_jets->begin(), selected_b_jets->end(), HelperFunctions::pt_sort());
  std::sort (signal_jets->begin(), signal_jets->end(), HelperFunctions::pt_sort());
}

//______________________________________________________________________________
//
void JetHelper::RetrieveClusters(const std::string& clus_key)
{
  const xAOD::CaloClusterContainer* clus = 0;
  if ( !m_event->retrieve( clus, clus_key ).isSuccess() ){ // retrieve arguments: container type, container key
    Error("execute()", "Failed to retrieve cluster container. Exiting." );
    return;
  }

  std::pair<xAOD::CaloClusterContainer*,xAOD::ShallowAuxContainer*> shallowcopy = xAOD::shallowCopyContainer(*clus);
  m_clus_copy = shallowcopy.first;
  m_clus_copy_aux = shallowcopy.second;

  return;
}

// //______________________________________________________________________________
// //
// void JetHelper::RetrieveFatJetsST(xAOD::JetContainer*& jets, xAOD::ShallowAuxContainer*& jets_aux){
//
//   m_event->copy("AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets");
//
//   if(!m_susy_tools->GetLargeJets(jets,jets_aux,false,"AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets").isSuccess()) std::cout << "Could not get the jets" << std::endl;
//
//   m_fj_copy = jets;
//   m_fj_copy_aux = jets_aux;
//
//   m_susy_tools->TopTags(jets);
//
//   jets_aux->setShallowIO( true );
//   m_event->record(jets, "FatJets");
//   m_event->record(jets_aux, "FatJetsAux.");
// }

//______________________________________________________________________________
//
void JetHelper::RetrieveFatJets(const std::string& fj_key,
        SubstructureTopTagger* top_tagger_smooth_tight,
        SubstructureTopTagger* top_tagger_smooth_loose,
        SubstructureTopTagger* top_tagger_lowpt_tight,
        SubstructureTopTagger* top_tagger_lowpt_loose,
        SubstructureTopTagger* top_tagger_highpt_tight,
        SubstructureTopTagger* top_tagger_highpt_loose)
{

  const xAOD::JetContainer* fj = 0;
  if ( !m_event->retrieve( fj, fj_key ).isSuccess() ){ // retrieve arguments: container type, container key
    Error("execute()", "Failed to retrieve fat jet container. Exiting." );
    return;
  }

  std::pair<xAOD::JetContainer*,xAOD::ShallowAuxContainer*> shallowcopy = xAOD::shallowCopyContainer(*fj);
  m_fj_copy = shallowcopy.first;
  m_fj_copy_aux = shallowcopy.second;

  if(!m_store->record(m_fj_copy, "FatJets").isSuccess()) std::cout << "Could not record fat jets" << std::endl;
  if(!m_store->record(m_fj_copy_aux, "FatJetsAux.").isSuccess()) std::cout << "Could not record fat jets aux" << std::endl;

  for ( const auto & ijet : *m_fj_copy ) {

    double pt = ijet->pt() / MEV;
    double eta = ijet->eta();

    // double m  = ijet->m()  / MEV;
    // double eta = ijet->eta();

    // double phi = ijet->phi();
    // double E   = ijet->e() / MEV;

    // double corr = 0;

    // int eta_bin = -1;

    // if(eta_const >= 0. && eta_const < 0.4)
    //   eta_bin = 0;
    // else if(eta_const >=0.4 && eta_const < 0.8)
    //   eta_bin = 1;
    // else if(eta_const >=0.8 && eta_const < 1.2)
    //   eta_bin = 2;
    // else if(eta_const >=1.2 && eta_const < 1.6)
    //   eta_bin = 3;
    // else if(eta_const >=1.6 && eta_const < 2.0)
    //   eta_bin = 4;

    if(eta > 2.0 || pt < 300.){
      // abort and decorate, we're out of the good region
      ijet->setAttribute("LooseSmoothTopTag",   -1);
      ijet->setAttribute("TightSmoothTopTag",   -1);
      ijet->setAttribute("LooseTopTag",         -1);
      ijet->setAttribute("TightTopTag",         -1);
      ijet->setAttribute("LooseHighPtTopTag",   -1);
      ijet->setAttribute("TightHighPtTopTag",   -1);
      ijet->setAttribute("LooseLowPtTopTag",    -1);
      ijet->setAttribute("TightLowPtTopTag",    -1);
      continue;
    }

    // std::cout << "have the eta bin " << eta_bin << std::endl;

    // TH2F* mass_calibration = massCalibs[eta_bin];

    // // std::cout << mass_calibration << std::endl;

    // double usedmass = m;
    // if(m >= 300)
    //   usedmass = 290.;

    // double usedpt = pt;
    // if(pt>=3000)
    //   usedpt = 2900.;

    // // std::cout << usedpt << " " << usedmass << std::endl;
    // corr = mass_calibration->Interpolate(usedpt, usedmass);

    // // std::cout << "corr is " << corr << std::endl;
    // if(corr==0)
    //   corr=1.;

    // double m_corr = m/corr;

    // // std::cout << "mass before " << m << " m after " << m_corr << std::endl;

    // double p = TMath::Sqrt(E*E - m_corr*m_corr);
    // pt = p / TMath::CosH(eta);

    // xAOD::JetFourMom_t jetMassP4;
    // jetMassP4.SetCoordinates(pt * MEV, eta, phi, m_corr * MEV);

    // ijet->setAttribute<xAOD::JetFourMom_t>("EtaMassJES",jetMassP4);
    // ijet->setJetP4(jetMassP4);

    // std::cout << " calibrated object is " << ijet->pt() << " " << ijet->m() << std::endl;


    if(top_tagger_smooth_tight){
      top_tagger_smooth_tight->doTag(*ijet);
    }

    if(top_tagger_smooth_loose){
      top_tagger_smooth_loose->doTag(*ijet);
    }

    if(top_tagger_lowpt_tight){
      top_tagger_lowpt_tight->doTag(*ijet);
    }

    if(top_tagger_lowpt_loose){
      top_tagger_lowpt_loose->doTag(*ijet);
    }

    if(top_tagger_highpt_tight){
      top_tagger_highpt_tight->doTag(*ijet);
    }

    if(top_tagger_highpt_loose){
      top_tagger_highpt_loose->doTag(*ijet);
    }

    if(top_tagger_lowpt_loose && top_tagger_highpt_loose){
      int is_loose = -1;
      if(ijet->pt() / MEV >= 300. && ijet->pt() / MEV <= 500.)
        ijet->getAttribute("LooseLowPtTopTag", is_loose);
      else if (ijet->pt() / MEV > 500.)
        ijet->getAttribute("LooseHighPtTopTag", is_loose);

      ijet->setAttribute("LooseTopTag", is_loose);
    }


    if(top_tagger_lowpt_tight && top_tagger_highpt_tight){
      int is_tight = -1;
      if(ijet->pt() / MEV >= 300. && ijet->pt() / MEV <= 500.)
        ijet->getAttribute("TightLowPtTopTag", is_tight);
      else if (ijet->pt() / MEV > 500.)
        ijet->getAttribute("TightHighPtTopTag", is_tight);

      ijet->setAttribute("TightTopTag", is_tight);
    }
  }

  return;
}

//______________________________________________________________________________
//
void JetHelper::RetrieveSignalFatJets(){
  // FatJetVector _signal_jets;
  ConstDataVector<xAOD::JetContainer> * selected_fat_jets = new ConstDataVector<xAOD::JetContainer>(SG::VIEW_ELEMENTS);

  int counter = 0;
  for(const auto jet_itr : *m_fj_copy) {
    counter++;
    if(jet_itr->pt() < 100000.) continue;
    if(fabs(jet_itr->eta()) > 2.) continue;
    selected_fat_jets->push_back(jet_itr);
  }

  if(!m_store->record(selected_fat_jets, "SelectedFatJets").isSuccess()) std::cout << "Could not record selected fat jets" << std::endl;

  // sort in pT
  std::sort (selected_fat_jets->begin(), selected_fat_jets->end(), HelperFunctions::pt_sort());
  m_fat_jets = selected_fat_jets->asDataVector();
}
