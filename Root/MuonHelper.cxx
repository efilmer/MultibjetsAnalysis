#include "MultibjetsAnalysis/MuonHelper.h"
#include "MultibjetsAnalysis/HelperFunctions.h"
#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "xAODBase/IParticleHelpers.h"
#include "xAODEventInfo/EventInfo.h"

//_________________________________________________________________________
//
MuonHelper::MuonHelper(ST::SUSYObjDef_xAOD*& susy_tools, xAOD::TEvent*& event, xAOD::TStore*& store) :
m_event(event),
m_store(store),
m_susy_tools(susy_tools),
m_nb_bad_muons(0),
m_nb_cosmic_muons(0){
  m_triggers.clear();
}

//_________________________________________________________________________
// Apply kinematic and quality cuts
void MuonHelper::DecorateBaselineMuons(xAOD::MuonContainer*& muons, const xAOD::EventInfo * /*info*/, std::string sys_name)
{

  m_nb_bad_muons = 0;
  m_nb_cosmic_muons = 0;

  std::string name = "SelectedMuons"+sys_name;
  ConstDataVector<xAOD::MuonContainer> * selected_muons =  new ConstDataVector<xAOD::MuonContainer>(SG::VIEW_ELEMENTS);
  if(!m_store->record( selected_muons, name ).isSuccess()) std::cout << "Could not record selected muons" << std::endl;

  std::string name_signal = "SignalMuons"+sys_name;
  ConstDataVector<xAOD::MuonContainer> * signal_muons   =  new ConstDataVector<xAOD::MuonContainer>(SG::VIEW_ELEMENTS);
  if(!m_store->record( signal_muons, name_signal ).isSuccess()) std::cout << "Could not record signal muons" << std::endl;

  static SG::AuxElement::ConstAccessor< char > baseline("baseline");
  static SG::AuxElement::ConstAccessor< char > passOR("passOR");
  static SG::AuxElement::ConstAccessor< char > bad("bad");
  static SG::AuxElement::ConstAccessor< char > cosmic("cosmic");
  static SG::AuxElement::ConstAccessor< char > signal("signal");

  for(const auto &muon : *muons) {
    //trigger matching decorations
    for( const auto trigger : m_triggers ){
      if( trigger.first.find("HLT_mu") == std::string::npos ) continue;
      muon->auxdata<char>("MBJ_trigger_matched"+trigger.first) = m_susy_tools -> IsTrigMatched(muon, trigger.first);
    }
    selected_muons->push_back(muon);

    // Get the number of bad muons before the OR, and cosmics after the OR
    if(!baseline.isAvailable(*muon) || baseline(*muon) == 0) continue;
    if(!bad.isAvailable(*muon) || bad(*muon) == 1) m_nb_bad_muons++;
    // Get the signal leptons before the OR
    if(signal.isAvailable(*muon) && signal(*muon) == 1){
      signal_muons->push_back(muon);
    }
    if(!passOR.isAvailable(*muon) || passOR(*muon) == 0) continue;
    if(!cosmic.isAvailable(*muon) || cosmic(*muon) == 1) m_nb_cosmic_muons++;
  }

  // sort in pT
  std::sort ( selected_muons->begin(), selected_muons->end(), HelperFunctions::pt_sort() );
  std::sort ( signal_muons->begin(), signal_muons->end(), HelperFunctions::pt_sort() );
}
