#include "MultibjetsAnalysis/Variables.h"
#include "MultibjetsAnalysis/HelperFunctions.h"

// P4Helpers
#include <FourMomUtils/xAODP4Helpers.h>

// for static casting
#include <xAODBase/IParticleContainer.h>

//______________________________________________________________________________
// Meff calculated with all jets with pt > 30 GeV and leptons with pt > 25 GeV
Float_t Variables::Meff_incl(const xAOD::MissingET* v_met, const xAOD::JetContainer* v_jets, const xAOD::MuonContainer*  v_muons, const xAOD::ElectronContainer*  v_electrons){

  Float_t meff = v_met->met() / MEV;
  for (auto jet : *v_jets) {
    meff += jet->pt() / MEV;
  }

  for (auto muon : *v_muons) {
    if(muon->auxdata<char>("signal")==0) continue;
    if(muon->pt() / MEV < 20.) continue;
    meff += muon->pt() / MEV;
  }


  for (auto elec : *v_electrons) {
    if(elec->auxdata<char>("signal")==0) continue;
    if(elec->pt() / MEV < 20.) continue;
    meff += elec->pt() / MEV;
  }

  return meff;
}

//______________________________________________________________________________
// Meff calculated with the n leading jets with pt > 30 GeV. The 4 leading jets are used by default
Float_t Variables::Meff_4j(const xAOD::MissingET* v_met, const xAOD::JetContainer* v_jets, u_int njets) {

  Float_t meff = v_met->met() / MEV;
  u_int n = 0;

  for (auto jet : *v_jets) {
    n++;
    if(n>njets) break;
    meff += jet->pt() / MEV;
  }

  return meff;
}

//______________________________________________________________________________
// Ht calculated with all jets with pt > 30 GeV and leptons with pt > 25 GeV
Float_t Variables::Ht(const xAOD::JetContainer* v_jets, const xAOD::MuonContainer*  v_muons,  const xAOD::ElectronContainer*  v_electrons){
  Float_t ht = 0;
  for (auto jet : *v_jets) {
    ht += jet->pt() / MEV;
  }

  for (auto muon : *v_muons) {
    if(muon->auxdata<char>("signal")==0) continue;
    if(muon->pt() / MEV < 20.) continue;
    ht += muon->pt() / MEV;
  }

  for (auto elec : *v_electrons) {
    if(elec->auxdata<char>("signal")==0) continue;
    if(elec->pt() / MEV < 20.) continue;
    ht += elec->pt() / MEV;
  }

  return ht;
}

//______________________________________________________________________________
//
Float_t Variables::MJSum(const xAOD::JetContainer* v_jets, const int maxjets){
  float mjsum(0.0);
  int njets(0);

  for (auto jet : *v_jets){
    if(njets>maxjets) continue;
    if(jet->pt() / MEV > 100.0 && fabs(jet->eta()) < 2.0){
      mjsum += (jet->m() / MEV);
    }
    njets++;
  }
  return mjsum;
}

//______________________________________________________________________________
// Met significance calculated with the n leading jets with pt > 30 GeV. The 4 leading jets are used by default
Float_t Variables::Met_significance(const xAOD::MissingET* v_met, const xAOD::JetContainer* v_jets, u_int njets){

  Float_t met_sig = 0;
  Float_t ht = 0;
  u_int n = 0;

  for (auto jet : *v_jets) {
    n++;
    if(n>njets) break;
    ht += jet->pt() / MEV;
  }

  float met = v_met->met() / MEV;
  if(ht!=0) met_sig = met / sqrt(ht);
  return met_sig;
}

//______________________________________________________________________________
// Transverse mass calculated with the leading lepton and the met
Float_t Variables::mT(const xAOD::MissingET* v_met, const xAOD::MuonContainer*  v_muons,  const xAOD::ElectronContainer*  v_electrons){

  float mt(0.0);

  // get leading lepton
  const xAOD::IParticle* leading_lepton(nullptr);

  for (auto muon : *v_muons) {
    if(muon->auxdata<char>("signal")==0) continue;
    leading_lepton = static_cast<const xAOD::IParticle*>(muon);
    break;
  }

  for (auto elec : *v_electrons) {
    if(elec->auxdata<char>("signal")==0) continue;
    if(!leading_lepton || elec->pt()>leading_lepton->pt()) leading_lepton = static_cast<const xAOD::IParticle*>(elec);
    break;
  }

  if(leading_lepton) mt = 2*leading_lepton->pt()*v_met->met()*(1-cos(xAOD::P4Helpers::deltaPhi(leading_lepton, v_met)));
  return sqrt(fabs(mt))/MEV;
}

//______________________________________________________________________________
// Minimum transverse mass of b-jets and the met with a mass set at 0 (default) or m(W)
Float_t Variables::mT_min_bjets(const xAOD::MissingET* v_met, const xAOD::JetContainer* v_jets, bool set_mw){

  Float_t mt_min = 100000.;
  Int_t   nb_bjets = 0;
  Float_t mw = 80.403;

  Float_t etw = v_met->met() / MEV;
  if(set_mw) etw = TMath::Hypot(v_met->met() / MEV, mw);

  static SG::AuxElement::ConstAccessor< char > bjet("bjet");

  for (auto jet : *v_jets) {
    if(!bjet.isAvailable(*jet) || bjet(*jet) == 0) continue;

    nb_bjets ++;
    if(nb_bjets>3) break;

    Float_t mt = 0;
    mt = pow(etw+jet->pt() / MEV,2) - pow(v_met->mpx() / MEV + jet->p4().Px() / MEV, 2) - pow(v_met->mpy() / MEV + jet->p4().Py() / MEV, 2);
    mt = (mt >= 0.) ? sqrt(mt) : sqrt(-mt);

    if(mt<mt_min) mt_min = mt;
  }

  if(nb_bjets==0) return 0;
  else return mt_min;
}

//______________________________________________________________________________
// delta phi min between met and the n leading jets
Float_t Variables::delta_phi_nj(const xAOD::MissingET* v_met, const xAOD::JetContainer* v_jets, u_int njets){

  Float_t dphi_min = 1000.;

  u_int n = 0;

  for (auto jet : *v_jets) {
    n++;
    if(n>njets) break;
    const xAOD::IParticle* IP_jet = static_cast<const xAOD::IParticle*>(jet);
    Float_t dphi_j = xAOD::P4Helpers::deltaPhi(IP_jet, v_met);
    if(fabs(dphi_j)<dphi_min) dphi_min = fabs(dphi_j);
  }
  return dphi_min;
}
