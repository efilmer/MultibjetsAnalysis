#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthParticle.h"

#include "NNLOReweighter/NNLOReweighter.h"

#include "MultibjetsAnalysis/TopTtbarPtReweighting.h"

//___________________________________________________
//
TopTtbarPtReweighting::TopTtbarPtReweighting( const int mc_channel_number, const bool useSysts ):
m_top(0),
m_antitop(0),
m_useSyst(useSysts),
m_systs(0),
m_ttbarNNLO_rw_tool(0)
{
    m_ttbarNNLO_rw_tool = new NNLOReweighter( mc_channel_number, (std::string(getenv("ROOTCOREBIN"))+"/data/NNLOReweighter/") );

    TFile* m_1l_file = TFile::Open(((std::string(getenv("ROOTCOREBIN"))+"/data/MultibjetsAnalysis/NNLO_PTavt_rew.root")).c_str());
    m_1l_reweight = (TH1F*) m_1l_file->Get("center")->Clone();
    m_1l_reweight->SetDirectory(0);
    m_1l_file->Close();
    delete m_1l_file;

    m_ttbarNNLO_rw_tool -> Init();
}

//___________________________________________________
//
TopTtbarPtReweighting::TopTtbarPtReweighting( const TopTtbarPtReweighting &q )
{
    m_top = q.m_top;
    m_antitop = q.m_antitop;
    m_useSyst = q.m_useSyst;
    m_systs = q.m_systs;
    m_ttbarNNLO_rw_tool = q.m_ttbarNNLO_rw_tool;
}

//___________________________________________________
//
TopTtbarPtReweighting::~TopTtbarPtReweighting()
{
    if(m_systs){
        m_systs -> clear();
        delete m_systs;
    }
    if(m_ttbarNNLO_rw_tool){
        delete m_ttbarNNLO_rw_tool;
    }
}

//___________________________________________________
//
bool TopTtbarPtReweighting::Clear()
{
    m_top = 0;
    m_antitop = 0;
    return true;
}

//___________________________________________________
//
bool TopTtbarPtReweighting::FindTops( const xAOD::TruthParticleContainer *vec )
{
    //Function to get the last top/antitop of the MC record (avoid intermediate tops)
    for ( const xAOD::TruthParticle* part : *vec ){
        if(part->pdgId()==6 && !m_top){
            if(!IsSelfDecayed(part)) m_top = part;
        } else if (part->pdgId()==-6 && !m_antitop){
            if(!IsSelfDecayed(part)) m_antitop = part;
        }
        if(m_top && m_antitop) break;//no need to go through the whole MC truth
    }
    return true;
}

//___________________________________________________
//
TopTtbarPtReweighting::TopTtbarPtReweightingResult TopTtbarPtReweighting::GetEventWeight() const
{
    double top_pt(0.),ttbar_pt(0);
    GetTopTtbarPt(top_pt, ttbar_pt);

    //Call to the funtion summarized here: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TtHbbRun2studies#NNLO_Reweighting
    TopTtbarPtReweighting::TopTtbarPtReweightingResult result;
    result.nominal = m_ttbarNNLO_rw_tool -> GetTtbarAndTopPtWeight( ttbar_pt * 1000., top_pt * 1000.);
    return result;
}

//___________________________________________________
//
void TopTtbarPtReweighting::GetTopTtbarPt( double &topPt, double &ttbarPt ) const
{
    //This function is used to retrieve some informations about the top-ttbar system
    if(m_top && m_antitop){
        topPt = m_top -> pt() / 1000.;
        xAOD::IParticle::FourMom_t ttbar = m_top->p4() + m_antitop->p4();
        ttbarPt = ttbar.Pt() / 1000.;
    } else {
        std::cerr << "<!> Request top and ttbar pT information ... but the top and anti-top have not yet been found ... Please check !" << std::endl;
    }
}

//___________________________________________________
//
bool TopTtbarPtReweighting::IsSelfDecayed( const xAOD::TruthParticle *part ) const
{
    const int nChildren = part -> nChildren();
    bool selfDecaying = false;
    for ( int iChild = 0; iChild < nChildren; ++iChild ){
        const xAOD::TruthParticle *child = part -> child( iChild );
        if( child && ( child -> pdgId() == part -> pdgId() ) ){
            selfDecaying = true;
            break;
        }
    }
    return selfDecaying;
}

//___________________________________________________
//
float TopTtbarPtReweighting::GetTop1LWeight(const double topPt ) const
{
  // const int bin = m_1l_reweight->FindBin(topPt);
  // const int n_bins = m_1l_reweight -> GetNbinsX();
  // const int used_bin = bin > n_bins ? n_bins : bin;
  // float m_1l_weight = m_1l_reweight->GetBinContent ( used_bin );
  // return m_1l_weight;

  //Call to the funtion summarized here: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TtHbbRun2studies#NNLO_Reweighting
  double weight = m_ttbarNNLO_rw_tool -> GetExtendedTopPtWeight( topPt * 1000.);
  return weight;
}
