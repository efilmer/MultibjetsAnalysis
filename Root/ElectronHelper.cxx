#include <MultibjetsAnalysis/ElectronHelper.h>
#include "SUSYTools/SUSYObjDef_xAOD.h"

#include "xAODEventInfo/EventInfo.h"
#include "xAODBase/IParticleHelpers.h"

#include "MultibjetsAnalysis/HelperFunctions.h"

//_________________________________________________________________________
//
ElectronHelper::ElectronHelper(ST::SUSYObjDef_xAOD*& susy_tools, xAOD::TEvent*& event, xAOD::TStore*& store) :
m_event(event),
m_store(store),
m_susy_tools(susy_tools)
{
  m_triggers.clear();
}

//_________________________________________________________________________
// Apply kinematic and quality cuts
void ElectronHelper::DecorateBaselineElectrons(xAOD::ElectronContainer*& electrons, const xAOD::EventInfo * /*info*/, std::string sys_name)
{
  std::string name = "SelectedElectrons"+sys_name;
  ConstDataVector<xAOD::ElectronContainer>* selected_electrons =  new ConstDataVector<xAOD::ElectronContainer>(SG::VIEW_ELEMENTS);
  if(!m_store->record( selected_electrons, name ).isSuccess()){
    std::cout << "Could not record selected electrons" << std::endl;
  }

  std::string name_signal = "SignalElectrons"+sys_name;
  ConstDataVector<xAOD::ElectronContainer>* signal_electrons =  new ConstDataVector<xAOD::ElectronContainer>(SG::VIEW_ELEMENTS);
  if(!m_store->record( signal_electrons, name_signal ).isSuccess()){
    std::cout << "Could not record signal electrons" << std::endl;
  }

  static SG::AuxElement::ConstAccessor< char > signal("signal");

  for(const auto &elec : *electrons) {
    //trigger matching decorations
    for( const auto trigger : m_triggers ){
      if( trigger.first.find("HLT_e") == std::string::npos ) continue;
      elec->auxdata<char>("MBJ_trigger_matched"+trigger.first) = m_susy_tools -> IsTrigMatched(elec, trigger.first);
    }
    selected_electrons->push_back(elec);
    if(!signal.isAvailable(*elec) || signal(*elec) == 0) continue;
    signal_electrons->push_back(elec);
  }

  // sort in pT
  std::sort(selected_electrons->begin(), selected_electrons->end(), HelperFunctions::pt_sort());
  std::sort(signal_electrons->begin(), signal_electrons->end(), HelperFunctions::pt_sort());
}
