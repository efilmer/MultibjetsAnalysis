#include "MultibjetsAnalysis/TreeMaker.h"
#include "MultibjetsAnalysis/Variables.h"
#include "MultibjetsAnalysis/TruthParticleHelper.h"
#include "MultibjetsAnalysis/HelperFunctions.h"

#include <utility>

// xAOD
#include "xAODEventInfo/EventInfo.h"
#include "xAODEgamma/Electron.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/ElectronAuxContainer.h"

#include "EventPrimitives/EventPrimitivesHelpers.h"
#include "xAODPrimitives/IsolationType.h"
#include "xAODEgamma/EgammaxAODHelpers.h"

// chiara
#include <TSystem.h>

#include "TRandom3.h"

#include<memory>

//_________________________________________________________________________
//
TreeMaker::TreeMaker(TFile *file, bool do_truth, bool do_rc_jets, bool do_vrc_jets, bool do_ak10_jets, bool do_TRF, std::vector<std::string> btagging_systematics, int TtbarHFCorrectionMode, bool do_bTagEff, bool doMM, bool do_syst, std::vector<ST::SystInfo> sys_list, const std::map< std::string, bool > &trigger_map ){

  TTree* tree_nom = new TTree("nominal", "nominal");
  tree_nom->SetAutoFlush(0);
  tree_nom->SetAutoSave(0);

  m_tree_map.insert( std::pair< std::string, TTree*> ("nominal" , tree_nom) );
  m_tree_map["nominal"]->SetDirectory (file);

  for(const auto& sys : sys_list) {
    const CP::SystematicSet& sysSet = sys.systset;
    const char* sys_name = sysSet.name().c_str();

    // create weights for weight systematics
    if((sysSet.name()).empty()) { // nominal
      m_weight_nom_map.insert( std::pair < const std::string, Double_t >("weight_mc",1.) );
      m_weight_nom_map.insert( std::pair < const std::string, Double_t >("weight_pu",1.) );
      m_weight_nom_map.insert( std::pair < const std::string, Double_t >("weight_btag",1.) );
      m_weight_nom_map.insert( std::pair < const std::string, Double_t >("weight_elec",1.) );
      m_weight_nom_map.insert( std::pair < const std::string, Double_t >("weight_elec_trigger",1.) );
      m_weight_nom_map.insert( std::pair < const std::string, Double_t >("weight_muon",1.) );
      m_weight_nom_map.insert( std::pair < const std::string, Double_t >("weight_muon_trigger",1.) );
      m_weight_nom_map.insert( std::pair < const std::string, Double_t >("weight_jvt",1.) );
      m_weight_nom_map.insert( std::pair < const std::string, Double_t >("weight_ttbar_NNLO",1.) );
      m_weight_nom_map.insert( std::pair < const std::string, Double_t >("weight_ttbar_NNLO_1L",1.) );
      m_weight_nom_map.insert( std::pair < const std::string, Double_t >("weight_WZ_2_2",1.) );
    } else if(!sys.affectsKinematics) { // systematics which affect the scale factors
      bool syst_affects_electrons = ST::testAffectsObject(xAOD::Type::Electron, sys.affectsType);
      bool syst_affects_muons     = ST::testAffectsObject(xAOD::Type::Muon, sys.affectsType);
      bool syst_affects_bTag      = ST::testAffectsObject(xAOD::Type::BTag, sys.affectsType);
      bool syst_affects_JVT       = (sysSet.name().find("Jvt") != std::string::npos);
      bool syst_affects_PU        = (sysSet.name().find("PRW") != std::string::npos);
      bool syst_is_trigger        = (sysSet.name().find("EFF_Trig") != std::string::npos);

      std::string trigger = "";
      if(syst_is_trigger) trigger = "trigger_";

      if(syst_affects_electrons)
      {
        const std::string name = "weight_elec_" +trigger + sysSet.name();
        m_weight_sys_map.insert( std::pair < const std::string, Double_t >(name,1.) );
      }
      if(syst_affects_muons)
      {
        const std::string name = "weight_muon_" +trigger + sysSet.name();
        m_weight_sys_map.insert( std::pair < const std::string, Double_t >(name,1.) );
      }
      if(syst_affects_bTag)
      {
        const std::string name = "weight_btag_" + sysSet.name();
        m_weight_sys_map.insert( std::pair < const std::string, Double_t >(name,1.) );
      }
      if(syst_affects_JVT)
			{
        const std::string name = "weight_jvt_" + sysSet.name();
        m_weight_sys_map.insert( std::pair < const std::string, Double_t >(name,1.) );
      }
      if(syst_affects_PU)
			{
        const std::string name = "weight_pu_" + sysSet.name();
        m_weight_sys_map.insert( std::pair < const std::string, Double_t >(name,1.) );
      }
    }
    else {
      // create trees for shape systematics
      if(do_syst) {
        TTree* tree_sys = new TTree(sys_name, sys_name);
        tree_sys->SetAutoFlush(0);
        tree_sys->SetAutoSave(0);
        m_tree_map.insert( std::pair< std::string, TTree*> (sys_name , tree_sys) );
        m_tree_map[sys_name]->SetDirectory (file);
      }
    }
  }

  for(auto &it : m_tree_map) {

    //
    // Event variables
    //
    it.second->Branch("event_number", &m_event_number, "event_number/l");
    it.second->Branch("run_number", &m_run_number, "run_number/l");
    it.second->Branch("random_run_number", &m_random_run_number, "random_run_number/l");
    it.second->Branch("lumiblock_number", &m_lumiblock_number, "lumiblock_number/I");
    it.second->Branch("average_interactions_per_crossing", &m_average_interactions_per_crossing, "average_interactions_per_crossing/F");
    it.second->Branch("primary_vtx_n", &m_n_vtx, "primary_vtx_n/I");
    it.second->Branch("actual_interactions_per_crossing", &m_actual_interactions_per_crossing, "actual_interactions_per_crossing/F");
    it.second->Branch("primary_vertex_z", &m_primary_vertex_z, "primary_vertex_z/F");
    it.second->Branch("process", &m_process, "process/I");
    it.second->Branch("gen_filt_ht", &m_gen_filt_ht, "gen_filt_ht/F");
    it.second->Branch("gen_filt_met", &m_gen_filt_met, "gen_filt_met/F");

    //
    // Muon variables
    //
    it.second->Branch("muons_n",&m_muons_n, "muons_n/I");
    it.second->Branch("muons_pt",&m_muons_pt);
    it.second->Branch("muons_phi",&m_muons_phi);
    it.second->Branch("muons_eta", &m_muons_eta);
    it.second->Branch("muons_e", &m_muons_e);
    it.second->Branch("muons_passOR", &m_muons_passOR);
    it.second->Branch("muons_isSignal", &m_muons_isSignal);
    it.second->Branch("muons_isCosmic", &m_muons_isCosmic);
    it.second->Branch("muons_isBad", &m_muons_isBad);
    it.second->Branch("muons_ptvarcone20", &m_muons_ptvarcone20);
    it.second->Branch("muons_ptvarcone30", &m_muons_ptvarcone30);
    it.second->Branch("muons_topoetcone20", &m_muons_topoetcone20);
    it.second->Branch("muons_d0sig", &m_muons_d0sig);
    it.second->Branch("muons_z0", &m_muons_z0);
    it.second->Branch("muons_charge", &m_muons_charge);
    //trigger matching
    for ( const std::pair < std::string, bool > trigger : trigger_map ){
      if( trigger.first.find("HLT_mu") == std::string::npos ) continue;
      m_mu_trigger.insert( std::pair < std::string, std::vector < int > >(trigger.first, {}) );
      it.second->Branch( ("muons_trgMatch_" + trigger.first).c_str(), &(m_mu_trigger[trigger.first]) );
    }

    //
    // Electron variables
    //
    it.second->Branch("electrons_n",&m_electrons_n, "electrons_n/I");
    it.second->Branch("electrons_pt",&m_electrons_pt);
    it.second->Branch("electrons_phi",&m_electrons_phi);
    it.second->Branch("electrons_eta", &m_electrons_eta);
    it.second->Branch("electrons_e", &m_electrons_e);
    it.second->Branch("electrons_passOR", &m_electrons_passOR);
    it.second->Branch("electrons_isSignal", &m_electrons_isSignal);
    it.second->Branch("electrons_passId", &m_electrons_passId);
    it.second->Branch("electrons_ptvarcone20", &m_electrons_ptvarcone20);
    it.second->Branch("electrons_ptvarcone30", &m_electrons_ptvarcone30);
    it.second->Branch("electrons_topoetcone20", &m_electrons_topoetcone20);
    it.second->Branch("electrons_d0sig", &m_electrons_d0sig);
    it.second->Branch("electrons_z0", &m_electrons_z0);
    it.second->Branch("electrons_charge", &m_electrons_charge);
    //trigger matching
    for ( const std::pair < std::string, bool > trigger : trigger_map ){
      if( trigger.first.find("HLT_e") == std::string::npos ) continue;
      m_el_trigger.insert( std::pair < std::string, std::vector < int > >(trigger.first, {}) );
      it.second->Branch( ("electrons_trgMatch_" + trigger.first).c_str(), &(m_el_trigger[trigger.first]) );
    }

    //
    // Jet variables
    //
    it.second->Branch("jets_n",&m_jets_n, "jets_n/I");
    it.second->Branch("jets_pt",&m_jets_pt);
    it.second->Branch("jets_phi",&m_jets_phi);
    it.second->Branch("jets_eta", &m_jets_eta);
    it.second->Branch("jets_e", &m_jets_e);
    it.second->Branch("jets_passOR", &m_jets_passOR);
    it.second->Branch("jets_isSignal", &m_jets_isSignal);
    it.second->Branch("jets_jvt", &m_jets_jvt);
    it.second->Branch("jets_nTracks", &m_jets_nTracks);
    it.second->Branch("jets_truthLabel", &m_jets_truthLabel);
    it.second->Branch("jets_btag_weight", &m_jets_btag_weight);
    it.second->Branch("jets_isb_60", &m_jets_isb_60);
    it.second->Branch("jets_isb_70", &m_jets_isb_70);
    it.second->Branch("jets_isb_77", &m_jets_isb_77);
    it.second->Branch("jets_isb_85", &m_jets_isb_85);
    it.second->Branch("jets_btagEff_weight", &m_jets_btagEff_weight);

    //
    // MET variables
    //
    it.second->Branch("metcst",&m_metcst,"metcst/F");
    it.second->Branch("metcst_phi",&m_metcst_phi,"metcst_phi/F");
    it.second->Branch("mettst",&m_mettst,"mettst/F");
    it.second->Branch("mettst_phi",&m_mettst_phi,"mettst_phi/F");
    it.second->Branch("metsoft",&m_metsoft,"metsoftj/F");
    it.second->Branch("metsoft_phi",&m_metsoft_phi,"metsoft_phi/F");

    //
    // Trigger-pass variables
    //
    for ( const std::pair < std::string, bool > trigger : trigger_map ){
      m_trigger.insert( std::pair < std::string, int >(trigger.first, 0) );
      it.second->Branch( ("pass_" + trigger.first).c_str(), &(m_trigger[trigger.first]), ("pass_" + trigger.first + "/I").c_str() );
    }

    //
    // Weight variables
    //
    for(auto &weight : m_weight_nom_map) {
      std::string name = weight.first;
      std::string branchName = name+"/D";
      it.second->Branch(name.c_str(), &(m_weight_nom_map[name]), branchName.c_str());
    }
    // add the weights for systematics only in the nominal tree
    if(it.first=="nominal") {
      for(auto &weight : m_weight_sys_map) {
        std::string name = weight.first;
        std::string branchName = name+"/D";
        it.second->Branch(name.c_str(), &(m_weight_sys_map[name]), branchName.c_str());
      }
    }

    //
    // Truth variables
    //
    if(do_truth) {
      it.second->Branch("ttbar_class",          &m_ttbar_class,         "ttbar_class/I");
      it.second->Branch("ttbar_class_ext",      &m_ttbar_class_ext,     "ttbar_class_ext/I");
      it.second->Branch("ttbar_class_prompt",   &m_ttbar_class_prompt,  "ttbar_class_prompt/I");
      it.second->Branch("top_decay_type",       &m_top_decay_type,      "top_decay_type/I");
      it.second->Branch("antitop_decay_type",   &m_antitop_decay_type,  "antitop_decay_type/I");
      it.second->Branch("ttbar_hf_filter_flag", &m_ttbar_hf_filter_flag,"ttbar_hf_filter_flag/I");

      it.second->Branch("met_truth",&m_met_truth,"met_truth/F");
      it.second->Branch("met_truth_phi",&m_met_truth_phi,"met_truth_phi/F");
      it.second->Branch("mtt",&m_mtt,"mtt/F");

      if(it.first=="nominal"){
        it.second->Branch("mc_n",&m_mc_n, "mc_n/I");
        it.second->Branch("mc_pt",&m_mc_pt);
        it.second->Branch("mc_eta",&m_mc_eta);
        it.second->Branch("mc_phi",&m_mc_phi);
        it.second->Branch("mc_m",&m_mc_m);
        it.second->Branch("mc_pdgId",&m_mc_pdgId);
        it.second->Branch("mc_children_index",&m_mc_children_index);
      }

      if(TtbarHFCorrectionMode>=2){
        std::vector < std::string > vec_ttbb_weights;
        vec_ttbb_weights.push_back("ttbb_Nominal_weight");
        if(TtbarHFCorrectionMode==3 && it.first=="nominal"){//consider systematic variations only for nominal tree
          vec_ttbb_weights.push_back("ttbb_CSS_KIN_weight");
          vec_ttbb_weights.push_back("ttbb_MSTW_weight");
          vec_ttbb_weights.push_back("ttbb_NNPDF_weight");
          vec_ttbb_weights.push_back("ttbb_Q_CMMPS_weight");
          vec_ttbb_weights.push_back("ttbb_glosoft_weight");
          vec_ttbb_weights.push_back("ttbb_defaultX05_weight");
          vec_ttbb_weights.push_back("ttbb_defaultX2_weight");
          vec_ttbb_weights.push_back("ttbb_MPIup_weight");
          vec_ttbb_weights.push_back("ttbb_MPIdown_weight");
          vec_ttbb_weights.push_back("ttbb_MPIfactor_weight");
          vec_ttbb_weights.push_back("ttbb_aMcAtNloHpp_weight");
          vec_ttbb_weights.push_back("ttbb_aMcAtNloPy8_weight");
        }
        for( const std::string sys : vec_ttbb_weights ){
          std::string name = "weight_ttbb_"+sys;
          std::string branchName = name+"/D";
          m_weight_ttbb.insert( std::pair < std::string, double >(name,1.) );
          it.second->Branch(name.c_str(), &(m_weight_ttbb[name]), branchName.c_str());
        }
        vec_ttbb_weights.clear();
      }
    }

    //
    // VRC jets variables
    //
    if(do_vrc_jets) {
      it.second->Branch("vrc_r10rho300_jets_n",&m_vrc_r10rho300_jets_n, "vrc_r10rho300_jets_n/I");
      it.second->Branch("vrc_r10rho300_jets_pt",&m_vrc_r10rho300_jets_pt);
      it.second->Branch("vrc_r10rho300_jets_phi",&m_vrc_r10rho300_jets_phi);
      it.second->Branch("vrc_r10rho300_jets_eta", &m_vrc_r10rho300_jets_eta);
      it.second->Branch("vrc_r10rho300_jets_e", &m_vrc_r10rho300_jets_e);
      it.second->Branch("vrc_r10rho300_jets_m", &m_vrc_r10rho300_jets_m);
      it.second->Branch("vrc_r10rho300_jets_Reff", &m_vrc_r10rho300_jets_Reff);
      it.second->Branch("vrc_r10rho300_jets_nconst", &m_vrc_r10rho300_jets_nconst);

      if(it.first=="nominal") {
	it.second->Branch("vrc_r10rho250_jets_n",&m_vrc_r10rho250_jets_n, "vrc_r10rho250_jets_n/I");
	it.second->Branch("vrc_r10rho250_jets_pt",&m_vrc_r10rho250_jets_pt);
	it.second->Branch("vrc_r10rho250_jets_phi",&m_vrc_r10rho250_jets_phi);
	it.second->Branch("vrc_r10rho250_jets_eta", &m_vrc_r10rho250_jets_eta);
	it.second->Branch("vrc_r10rho250_jets_e", &m_vrc_r10rho250_jets_e);
	it.second->Branch("vrc_r10rho250_jets_m", &m_vrc_r10rho250_jets_m);
	it.second->Branch("vrc_r10rho250_jets_Reff", &m_vrc_r10rho250_jets_Reff);
	it.second->Branch("vrc_r10rho250_jets_nconst", &m_vrc_r10rho250_jets_nconst);

	it.second->Branch("vrc_r10rho350_jets_n",&m_vrc_r10rho350_jets_n, "vrc_r10rho350_jets_n/I");
	it.second->Branch("vrc_r10rho350_jets_pt",&m_vrc_r10rho350_jets_pt);
	it.second->Branch("vrc_r10rho350_jets_phi",&m_vrc_r10rho350_jets_phi);
	it.second->Branch("vrc_r10rho350_jets_eta", &m_vrc_r10rho350_jets_eta);
	it.second->Branch("vrc_r10rho350_jets_e", &m_vrc_r10rho350_jets_e);
	it.second->Branch("vrc_r10rho350_jets_m", &m_vrc_r10rho350_jets_m);
	it.second->Branch("vrc_r10rho350_jets_Reff", &m_vrc_r10rho350_jets_Reff);
	it.second->Branch("vrc_r10rho350_jets_nconst", &m_vrc_r10rho350_jets_nconst);

        it.second->Branch("vrc_r12rho300_jets_n",&m_vrc_r12rho300_jets_n, "vrc_r12rho300_jets_n/I");
        it.second->Branch("vrc_r12rho300_jets_pt",&m_vrc_r12rho300_jets_pt);
        it.second->Branch("vrc_r12rho300_jets_phi",&m_vrc_r12rho300_jets_phi);
        it.second->Branch("vrc_r12rho300_jets_eta", &m_vrc_r12rho300_jets_eta);
        it.second->Branch("vrc_r12rho300_jets_e", &m_vrc_r12rho300_jets_e);
        it.second->Branch("vrc_r12rho300_jets_m", &m_vrc_r12rho300_jets_m);
        it.second->Branch("vrc_r12rho300_jets_Reff", &m_vrc_r12rho300_jets_Reff);
        it.second->Branch("vrc_r12rho300_jets_nconst", &m_vrc_r12rho300_jets_nconst);
      }//Only add these branches in nominal tree

    }

    //
    // RC jets variables
    //
    if(do_rc_jets) {
      it.second->Branch("rc_R10PT05_jets_n",&m_rc_R10PT05_jets_n, "rc_R10PT05_jets_n/I");
      it.second->Branch("rc_R10PT05_jets_pt",&m_rc_R10PT05_jets_pt);
      it.second->Branch("rc_R10PT05_jets_phi",&m_rc_R10PT05_jets_phi);
      it.second->Branch("rc_R10PT05_jets_eta", &m_rc_R10PT05_jets_eta);
      it.second->Branch("rc_R10PT05_jets_e", &m_rc_R10PT05_jets_e);
      it.second->Branch("rc_R10PT05_jets_m", &m_rc_R10PT05_jets_m);
      it.second->Branch("rc_R10PT05_jets_nconst", &m_rc_R10PT05_jets_nconst);
    }

    //
    // Standard anti-kT 10 jets variables
    //
    if(do_ak10_jets) {
      it.second->Branch("ak10_jets_n",&m_ak10_jets_n, "ak10_jets_n/I");
      it.second->Branch("ak10_jets_pt",&m_ak10_jets_pt);
      it.second->Branch("ak10_jets_phi",&m_ak10_jets_phi);
      it.second->Branch("ak10_jets_eta", &m_ak10_jets_eta);
      it.second->Branch("ak10_jets_e", &m_ak10_jets_e);
      it.second->Branch("ak10_jets_m", &m_ak10_jets_m);
      it.second->Branch("ak10_jets_SPLIT12", &m_ak10_jets_SPLIT12);
      it.second->Branch("ak10_jets_SPLIT23", &m_ak10_jets_SPLIT23);
      it.second->Branch("ak10_jets_tau21", &m_ak10_jets_Tau21);
      it.second->Branch("ak10_jets_tau32", &m_ak10_jets_Tau32);
      it.second->Branch("ak10_jets_isTopLoose", &m_ak10_jets_isTopLoose);
      it.second->Branch("ak10_jets_isTopTight", &m_ak10_jets_isTopTight);
      it.second->Branch("ak10_jets_isTopSmoothLoose", &m_ak10_jets_isTopSmoothLoose);
      it.second->Branch("ak10_jets_isTopSmoothTight", &m_ak10_jets_isTopSmoothTight);
    }

    // b-tagging efficiencies for the matrix method
    if(do_bTagEff || doMM) {
      m_bTag_tool = new BTaggingEfficiencyTool("BTagTool");
      m_bTag_tool->setProperty("TaggerName", "MV2c20");
      m_bTag_tool->setProperty("OperatingPoint", "-0_7887");
      m_bTag_tool->setProperty("JetAuthor", "AntiKt4EMTopoJets");
      m_bTag_tool->setProperty("ScaleFactorFileName", "xAODBTaggingEfficiency/13TeV/2015-PreRecomm-13TeV-MC12-CDI_August3-v1.root");
      // specify the use of the cone-based labelling rather than the ghost association
      m_bTag_tool->setProperty("ConeFlavourLabel", true);
      // uncomment this if the "Envelope" systematics model is to be used instead of the eigenvector variations
      m_bTag_tool->setProperty("SystematicsStrategy", "Envelope");
      StatusCode code = m_bTag_tool->initialize();
      if (code != StatusCode::SUCCESS) {
        std::cout << "Initialization of tool " << m_bTag_tool->name() << " failed! Subsequent results may not make sense." << std::endl;
      }
    }

    if(do_bTagEff){
      it.second->Branch("jets_bTagEff_85", &m_jets_bTagEff_85);
      it.second->Branch("jets_bTagEff_85_down", &m_jets_bTagEff_85_down);
      it.second->Branch("jets_bTagEff_85_up", &m_jets_bTagEff_85_up);
    }

    // matrix method weights
    if(doMM){
      m_MM = new MatrixMethod_bjets();
      char* path = getenv("ROOTCOREBIN");
      std::string maindir = std::string(path) + "/data/";
      m_MM->initialize(maindir+"MultibjetsAnalysis/MM_test_TCL_7bins.root");

      it.second->Branch("MM_weight_85_30", &m_MM_weight_85_30);
      it.second->Branch("MM_weight_85_30_bd", &m_MM_weight_85_30_bd);
      it.second->Branch("MM_weight_85_30_cd", &m_MM_weight_85_30_cd);
      it.second->Branch("MM_weight_85_30_ld", &m_MM_weight_85_30_ld);
      it.second->Branch("MM_weight_85_30_bu", &m_MM_weight_85_30_bu);
      it.second->Branch("MM_weight_85_30_cu", &m_MM_weight_85_30_cu);
      it.second->Branch("MM_weight_85_30_lu", &m_MM_weight_85_30_lu);
      it.second->Branch("MM_weight_85_30_MCstat", &m_MM_weight_85_30_MCstat);
      it.second->Branch("MM_weight_85_30_data", &m_MM_weight_85_30_data);

      it.second->Branch("MM_weight_85_50", &m_MM_weight_85_50);
      it.second->Branch("MM_weight_85_50_bd", &m_MM_weight_85_50_bd);
      it.second->Branch("MM_weight_85_50_cd", &m_MM_weight_85_50_cd);
      it.second->Branch("MM_weight_85_50_ld", &m_MM_weight_85_50_ld);
      it.second->Branch("MM_weight_85_50_bu", &m_MM_weight_85_50_bu);
      it.second->Branch("MM_weight_85_50_cu", &m_MM_weight_85_50_cu);
      it.second->Branch("MM_weight_85_50_lu", &m_MM_weight_85_50_lu);
      it.second->Branch("MM_weight_85_50_MCstat", &m_MM_weight_85_50_MCstat);
      it.second->Branch("MM_weight_85_50_data", &m_MM_weight_85_50_data);

      it.second->Branch("MM_weight_85_70", &m_MM_weight_85_70);
      it.second->Branch("MM_weight_85_70_bd", &m_MM_weight_85_70_bd);
      it.second->Branch("MM_weight_85_70_cd", &m_MM_weight_85_70_cd);
      it.second->Branch("MM_weight_85_70_ld", &m_MM_weight_85_70_ld);
      it.second->Branch("MM_weight_85_70_bu", &m_MM_weight_85_70_bu);
      it.second->Branch("MM_weight_85_70_cu", &m_MM_weight_85_70_cu);
      it.second->Branch("MM_weight_85_70_lu", &m_MM_weight_85_70_lu);
      it.second->Branch("MM_weight_85_70_MCstat", &m_MM_weight_85_70_MCstat);
      it.second->Branch("MM_weight_85_70_data", &m_MM_weight_85_70_data);

      it.second->Branch("MM_weight_85_90", &m_MM_weight_85_90);
      it.second->Branch("MM_weight_85_90_bd", &m_MM_weight_85_90_bd);
      it.second->Branch("MM_weight_85_90_cd", &m_MM_weight_85_90_cd);
      it.second->Branch("MM_weight_85_90_ld", &m_MM_weight_85_90_ld);
      it.second->Branch("MM_weight_85_90_bu", &m_MM_weight_85_90_bu);
      it.second->Branch("MM_weight_85_90_cu", &m_MM_weight_85_90_cu);
      it.second->Branch("MM_weight_85_90_lu", &m_MM_weight_85_90_lu);
      it.second->Branch("MM_weight_85_90_MCstat", &m_MM_weight_85_90_MCstat);
      it.second->Branch("MM_weight_85_90_data", &m_MM_weight_85_90_data);
    }

    if(do_TRF){
      it.second->Branch( "truthTagisB_2bex", &m_perm_2bex );
      it.second->Branch( "truthTagisB_3bex", &m_perm_3bex );
      it.second->Branch( "truthTagisB_4bin", &m_perm_4bin );
      for(auto bsyst : btagging_systematics){
        if(bsyst!="Nominal"){
          if(!do_syst) continue;
          if(it.first!="nominal") continue;
        }
        std::string name_2bex = "truthTagWei_" + bsyst + "_2bex";
        std::string name_3bex = "truthTagWei_" + bsyst + "_3bex";
        std::string name_4bin = "truthTagWei_" + bsyst + "_4bin";
        m_ttwei_2bex.insert( std::pair < std::string, double >(bsyst, 0.) );
        m_ttwei_3bex.insert( std::pair < std::string, double >(bsyst, 0.) );
        m_ttwei_4bin.insert( std::pair < std::string, double >(bsyst, 0.) );
        it.second->Branch( name_2bex.c_str(), &(m_ttwei_2bex[bsyst]) );
        it.second->Branch( name_3bex.c_str(), &(m_ttwei_3bex[bsyst]) );
        it.second->Branch( name_4bin.c_str(), &(m_ttwei_4bin[bsyst]) );
      }
    }
    it.second->Branch("truth_jets_n", &m_truth_jets_n);
  }

  m_rdm = new TRandom3();

}

//_________________________________________________________________________
//
TreeMaker::~TreeMaker(){
}

//_________________________________________________________________________
//
void TreeMaker::Fill_obj(unsigned long long event_number, long int run_number, long int random_run_number, float average_interactions_per_crossing,  float actual_interactions_per_crossing,  float primary_vertex_z, int n_primary_vertices, int process,  int ttbar_class,  int ttbar_class_ext, int ttbar_class_prompt, std::map < std::string, float > &ttbb_weight_map,  int top_decay_type,  int antitop_decay_type,  const xAOD::MuonContainer* v_muons,  const xAOD::ElectronContainer* v_electrons,  const xAOD::JetContainer* v_jets,  const xAOD::MissingETContainer* v_metcst,  const xAOD::MissingETContainer* v_mettst,  const std::map< std::string, bool > &v_trigger,  std::map < const std::string, Double_t > weight_map,  bool do_TRF,const TTres* ttres, const xAOD::EventInfo* info, xAOD::TEvent*& event)
{
  Reset();

  bool isMC = false;
  // check if the event is MC
  if(info->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
    isMC = true; // can do something with this later
  }
  m_event_number         = event_number;
  m_run_number           = run_number;
  m_random_run_number    = random_run_number;
  m_lumiblock_number     = info->lumiBlock();
  m_average_interactions_per_crossing = average_interactions_per_crossing;
  m_actual_interactions_per_crossing  = actual_interactions_per_crossing;
  m_primary_vertex_z     = primary_vertex_z;
  m_process              = process;
  m_n_vtx                = n_primary_vertices;

  static SG::AuxElement::ConstAccessor<float> GenFiltHT_d("ht_filter");
  static SG::AuxElement::ConstAccessor<float> GenFiltMET_d("met_filter");
  static SG::AuxElement::ConstAccessor<int> GenTtbarFiltHfFlag_d("TopHeavyFlavorFilterFlag");
  m_gen_filt_ht  = 0.0;
  m_gen_filt_met = 0.0;
  if(GenFiltHT_d.isAvailable(*info)) m_gen_filt_ht = GenFiltHT_d(*info);
  if(GenFiltMET_d.isAvailable(*info)) m_gen_filt_met = GenFiltMET_d(*info);

  m_ttbar_class  = ttbar_class;
  m_ttbar_class_ext  = ttbar_class_ext;
  m_ttbar_class_prompt  = ttbar_class_prompt;
  m_top_decay_type = top_decay_type;
  m_antitop_decay_type = antitop_decay_type;
  if(GenTtbarFiltHfFlag_d.isAvailable(*info)) m_ttbar_hf_filter_flag = GenTtbarFiltHfFlag_d(*info);

  xAOD::MissingETContainer::const_iterator metcst_it = v_metcst->find("Final");
  if (metcst_it == v_metcst->end()) std::cout << "No RefFinal inside MET container" << std::endl;
  xAOD::MissingET* metcst = *metcst_it;

  xAOD::MissingETContainer::const_iterator mettst_it = v_mettst->find("Final");
  if (mettst_it == v_mettst->end()) std::cout << "No RefFinal inside MET container" << std::endl;
  xAOD::MissingET* mettst = *mettst_it;

  xAOD::MissingETContainer::const_iterator met_it_soft = v_mettst->find("PVSoftTrk");
  if (met_it_soft == v_mettst->end()) std::cout << "No TST inside MET container" << std::endl;
  xAOD::MissingET* metsoft = *met_it_soft;

  m_metcst      = metcst->met() / MEV;
  m_metcst_phi  = metcst->phi();
  m_mettst      = mettst->met() / MEV;
  m_mettst_phi  = mettst->phi();
  m_metsoft     = metsoft->met() / MEV;
  m_metsoft_phi = metsoft->phi();

  // default met is mtst
  m_meff = Variables::Meff_incl(mettst, v_jets, v_muons, v_electrons);
  m_ht = Variables::Ht(v_jets, v_muons, v_electrons);
  m_met_sig = Variables::Met_significance(mettst, v_jets, 4);
  m_mt = Variables::mT(mettst, v_muons, v_electrons);
  m_mt_min_bmet = Variables::mT_min_bjets(mettst, v_jets, false);
  m_mt_min_bmetW = Variables::mT_min_bjets(mettst, v_jets, true);

  for ( std::pair < const std::string, Double_t > weight : weight_map ) {
    if ( m_weight_nom_map.find(weight.first) != m_weight_nom_map.end()) m_weight_nom_map[weight.first] = weight.second;
    if ( m_weight_sys_map.find(weight.first) != m_weight_sys_map.end()) m_weight_sys_map[weight.first] = weight.second;
  }

	//#######################################################
	// Muons
	//#######################################################
  m_muons_n = 0;
  for (auto muon : *v_muons) {
    if(muon->auxdata<char>("baseline")==0) continue;
    m_muons_n ++;

		//Kinematics
    m_muons_pt.push_back(muon->pt() / MEV);
    m_muons_eta.push_back(muon->eta());
    m_muons_phi.push_back(muon->phi());
    m_muons_e.push_back(muon->e() / MEV);
    m_muons_charge.push_back(muon->charge());

		//Some more selection criteria
    m_muons_passOR.push_back((muon->auxdata<char>("passOR")==1));
    m_muons_isSignal.push_back((muon->auxdata<char>("signal")==1));
    m_muons_isCosmic.push_back((muon->auxdata<char>("cosmic")==1));
    m_muons_isBad.push_back((muon->auxdata<char>("bad")==1));

		//Isolation variables
    m_muons_ptvarcone20.push_back(muon->auxdata<float>("ptvarcone20") / MEV);
    m_muons_ptvarcone30.push_back(muon->auxdata<float>("ptvarcone30") / MEV);
    m_muons_topoetcone20.push_back(muon->auxdata<float>("topoetcone20") / MEV);

		//Tracking
    m_muons_d0sig.push_back(HelperFunctions::getD0sig(muon, info));
    m_muons_z0.push_back(HelperFunctions::getZ0(muon, primary_vertex_z));

		//Trigger matching decisions
    for( auto trigger : v_trigger) {
      if( trigger.first.find("HLT_mu") == std::string::npos ) continue;
      m_mu_trigger[trigger.first].push_back(muon->auxdata<char>("MBJ_trigger_matched"+trigger.first)==1);
    }
  }

	//#######################################################
	// Electrons
	//#######################################################
  m_electrons_n = 0;
  for (auto electron : *v_electrons) {
    if(electron->auxdata<char>("baseline")==0) continue;
    m_electrons_n ++;

		//Kinematics
    m_electrons_pt.push_back(electron->pt() / MEV);
    m_electrons_eta.push_back(electron->eta());
    m_electrons_phi.push_back(electron->phi());
    m_electrons_e.push_back(electron->e() / MEV);
    m_electrons_charge.push_back(electron->charge());

		//Selection criteria
    m_electrons_passOR.push_back((electron->auxdata<char>("passOR")==1));
    m_electrons_isSignal.push_back((electron->auxdata<char>("signal")==1));
    m_electrons_passId.push_back((electron->auxdata<char>("passSignalID")==1));

		//Isolation variables
    Float_t ptvarcone20 = 0;
    electron->isolationValue(ptvarcone20,xAOD::Iso::ptvarcone20);
    m_electrons_ptvarcone20.push_back(ptvarcone20 / MEV);

    Float_t ptvarcone30 = 0;
    electron->isolationValue(ptvarcone30,xAOD::Iso::ptvarcone30);
    m_electrons_ptvarcone30.push_back(ptvarcone30 / MEV);

    Float_t topoetcone20 = 0;
    electron->isolationValue(topoetcone20,xAOD::Iso::topoetcone20);
    m_electrons_topoetcone20.push_back(topoetcone20 / MEV);

		//Tracking variables
    m_electrons_d0sig.push_back(HelperFunctions::getD0sig(electron, info));
    m_electrons_z0.push_back(HelperFunctions::getZ0(electron, primary_vertex_z));

		//Trigger matching decision
    for( auto trigger : v_trigger) {
      if( trigger.first.find("HLT_e") == std::string::npos ) continue;
      m_el_trigger[trigger.first].push_back(electron->auxdata<char>("MBJ_trigger_matched"+trigger.first)==1);
    }
  }

	//#######################################################
	// Jets
	//#######################################################
  m_jets_n = 0;
  for (auto jet : *v_jets) {
    m_jets_n++;

		//Kinematics
    m_jets_pt.push_back(jet->pt() / MEV);
    m_jets_eta.push_back(jet->eta());
    m_jets_phi.push_back(jet->phi());
    m_jets_e.push_back(jet->e() / MEV);

		//Other selection criteria
    m_jets_passOR.push_back((jet->auxdata<char>("passOR")==1));
    m_jets_isBad.push_back((jet->auxdata<char>("bad")==1));
    m_jets_isSignal.push_back((jet->auxdata<char>("signal")==1));

    static SG::AuxElement::ConstAccessor<float> cacc_jvt("Jvt");
    m_jets_jvt.push_back(cacc_jvt(*jet));

    int nTracks = -1;
    std::vector<int> nTrkVec;
    jet->getAttribute(xAOD::JetAttribute::NumTrkPt500, nTrkVec);
    m_jets_nTracks.push_back(nTracks);

    // B-Tagging: might improve this a bit...
    Int_t truth_label = -1;
    if(isMC) jet->getAttribute("HadronConeExclTruthLabelID", truth_label);
    m_jets_truthLabel.push_back(truth_label);

    double btag_weight = -99.;
    (jet->btagging())->MVx_discriminant("MV2c10", btag_weight);
    m_jets_btag_weight.push_back(btag_weight);
    m_jets_isb_60.push_back(btag_weight > 0.934906);
    m_jets_isb_70.push_back(btag_weight > 0.8244273);
    m_jets_isb_77.push_back(btag_weight > 0.645925);
    m_jets_isb_85.push_back(btag_weight > 0.1758475);

    if(isMC) m_jets_btagEff_weight.push_back(jet->auxdata<double>("effscalefact"));
  }

  if(ttbb_weight_map.size()>0){
    for( const std::pair < std::string, double > syst : ttbb_weight_map ){
      std::string name = "weight_ttbb_"+syst.first;
      m_weight_ttbb[name] = syst.second;
    }
  }

  for( auto trigger : v_trigger) {
    m_trigger[trigger.first] = trigger.second ? 1 : 0;
  }

  // TRF
  if(do_TRF){
    //permutations
    m_perm_2bex = ttres->perm_ex.at(2);
    m_perm_3bex = ttres->perm_ex.at(3);
    m_perm_4bin = ttres->perm_in.at(4);
    //weights
    for( const auto &wei_ex : ttres->ttwei_ex ){
      //2bex
      m_ttwei_2bex[wei_ex.first] = (wei_ex.second).at(2);
      //3bex
      m_ttwei_3bex[wei_ex.first] = (wei_ex.second).at(3);
    }
    for( const auto &wei_in : ttres->ttwei_in ){
      //4bin
      m_ttwei_4bin[wei_in.first] = (wei_in.second).at(4);
    }
  }


  if(isMC){
    m_truth_jets_n = 0;
    const xAOD::JetContainer* truth_jets = 0;
    if(!event->retrieve(truth_jets, "AntiKt4TruthWZJets").isSuccess()){
      std::cout << "<!> Problem retreiving the truth jets ! " << std::endl;
    } else {
      for( const auto *tj : *truth_jets ){
        if(tj->pt()<20000) continue;
        if(TMath::Abs(tj->eta())>2.8) continue;
        m_truth_jets_n++;
      }
    }
  }
}

//_________________________________________________________________________
//
void TreeMaker::Fill_truth( const xAOD::TruthParticleContainer *v_truth, TLorentzVector v_met_truth )
{
  m_mc_n = 0;
  for ( const auto &mc : * v_truth) {

    m_mc_n++;

    m_mc_pt.push_back(mc->pt()/1000.);
    m_mc_eta.push_back(mc->eta());
    m_mc_phi.push_back(mc->phi());
    m_mc_m.push_back(mc->m()/1000.);
    m_mc_status.push_back(mc->status());
    m_mc_pdgId.push_back(mc->pdgId());

    //
    // Children
    //
    std::vector < int > ind_children;
    const unsigned int nOutgoing = mc -> auxdecor < int >("children_n");
    for ( unsigned int iOut = 0; iOut < nOutgoing; ++iOut) {
      std::string key = "child_index" + std::to_string(iOut+1);
      ind_children.push_back(mc -> auxdecor < int > (key));
    }
    m_mc_children_index.push_back(ind_children);
  }

  m_met_truth     = v_met_truth.Pt();
  m_met_truth_phi = v_met_truth.Phi();

  // mtt for the ttbar mtt sliced samples
  m_mtt =  TruthParticleHelper::mtt(m_mc_pt, m_mc_eta, m_mc_phi, m_mc_pdgId, m_mc_status);

}


//_________________________________________________________________________
//
void TreeMaker::Fill_vrc_jets(xAOD::TStore*& m_store, std::string sys_name, bool is_nominal, bool affects_jets, int /*maxjets_mjsum*/)
{
  std::string vrc_r10rho350_name = "VRCJets_r10rho350";
  std::string vrc_r10rho300_name = "VRCJets_r10rho300";
  std::string vrc_r12rho300_name = "VRCJets_r12rho300";
  std::string vrc_r10rho250_name = "VRCJets_r10rho250";

  m_vrc_r10rho350_jets_n = 0;
  m_vrc_r10rho300_jets_n = 0;
  m_vrc_r12rho300_jets_n = 0;
  m_vrc_r10rho250_jets_n = 0;

  if(!is_nominal && affects_jets){
    vrc_r10rho350_name += sys_name;
    vrc_r10rho300_name += sys_name;
    vrc_r12rho300_name += sys_name;
    vrc_r10rho250_name += sys_name;
  }

  //
  // -- R = 1.0, rho = 300 GeV
  //
  const xAOD::JetContainer* vrc_r10rho300_jets = 0;
  const xAOD::JetAuxContainer* vrc_r10rho300_jets_aux = 0;
  if(!m_store->retrieve(vrc_r10rho300_jets, vrc_r10rho300_name).isSuccess()){
    std::cout << "TreeMaker could not retrieve " << vrc_r10rho300_name << std::endl;
  }
  if(!m_store->retrieve(vrc_r10rho300_jets_aux, vrc_r10rho300_name + "Aux.").isSuccess()){
    std::cout << "TreeMaker could not retrieve " << vrc_r10rho300_name << "Aux." << std::endl;
  }
  m_vrc_r10rho300_jets_n = vrc_r10rho300_jets->size();
  for (auto jet : *vrc_r10rho300_jets) {
    m_vrc_r10rho300_jets_pt.push_back(jet->pt() / MEV);
    m_vrc_r10rho300_jets_eta.push_back(jet->eta());
    m_vrc_r10rho300_jets_phi.push_back(jet->phi());
    m_vrc_r10rho300_jets_e.push_back(jet->e() / MEV);
    m_vrc_r10rho300_jets_m.push_back(jet->m() / MEV);

    const ElementLink<xAOD::JetContainer> pparent(jet->getAttribute<ElementLink<xAOD::JetContainer> >("Parent"));
    const xAOD::Jet* parent(*pparent);
    m_vrc_r10rho300_jets_Reff.push_back(parent->getAttribute<float>("EffectiveR"));

    //m_vrc_r10rho300_jets_m.push_back(jet->getAttribute<float>("EffectiveR"));
    m_vrc_r10rho300_jets_nconst.push_back(jet->numConstituents());
  }

  //Only run for nominal
  if(is_nominal){

    //
    // -- R = 1.0, rho = 350 GeV
    //
    const xAOD::JetContainer* vrc_r10rho350_jets = 0;
    const xAOD::JetAuxContainer* vrc_r10rho350_jets_aux = 0;
    if(!m_store->retrieve(vrc_r10rho350_jets, vrc_r10rho350_name).isSuccess()) {
      std::cout << "TreeMaker could not retrieve " << vrc_r10rho350_name << std::endl;
    }
    if(!m_store->retrieve(vrc_r10rho350_jets_aux, vrc_r10rho350_name + "Aux.").isSuccess()){
      std::cout << "TreeMaker could not retrieve " << vrc_r10rho350_name << "Aux." << std::endl;
    }
    m_vrc_r10rho350_jets_n = vrc_r10rho350_jets->size();
    for (auto jet : *vrc_r10rho350_jets) {
      m_vrc_r10rho350_jets_pt.push_back(jet->pt() / MEV);
      m_vrc_r10rho350_jets_eta.push_back(jet->eta());
      m_vrc_r10rho350_jets_phi.push_back(jet->phi());
      m_vrc_r10rho350_jets_e.push_back(jet->e() / MEV);
      m_vrc_r10rho350_jets_m.push_back(jet->m() / MEV);

      const ElementLink<xAOD::JetContainer> pparent(jet->getAttribute<ElementLink<xAOD::JetContainer> >("Parent"));
      const xAOD::Jet* parent(*pparent);
      m_vrc_r10rho350_jets_Reff.push_back(parent->getAttribute<float>("EffectiveR"));

      //m_vrc_r10rho350_jets_m.push_back(jet->getAttribute<float>("EffectiveR"));
      m_vrc_r10rho350_jets_nconst.push_back(jet->numConstituents());
    }

    //
    // -- R = 1.0, rho = 250 GeV
    //
    const xAOD::JetContainer* vrc_r10rho250_jets = 0;
    const xAOD::JetAuxContainer* vrc_r10rho250_jets_aux = 0;
    if(!m_store->retrieve(vrc_r10rho250_jets, vrc_r10rho250_name).isSuccess()){
      std::cout << "TreeMaker could not retrieve " << vrc_r10rho250_name << std::endl;
    }
    if(!m_store->retrieve(vrc_r10rho250_jets_aux, vrc_r10rho250_name + "Aux.").isSuccess()){
      std::cout << "TreeMaker could not retrieve " << vrc_r10rho250_name << "Aux." << std::endl;
    }
    m_vrc_r10rho250_jets_n = vrc_r10rho250_jets->size();
    for (auto jet : *vrc_r10rho250_jets) {
      m_vrc_r10rho250_jets_pt.push_back(jet->pt() / MEV);
      m_vrc_r10rho250_jets_eta.push_back(jet->eta());
      m_vrc_r10rho250_jets_phi.push_back(jet->phi());
      m_vrc_r10rho250_jets_e.push_back(jet->e() / MEV);
      m_vrc_r10rho250_jets_m.push_back(jet->m() / MEV);

      const ElementLink<xAOD::JetContainer> pparent(jet->getAttribute<ElementLink<xAOD::JetContainer> >("Parent"));
      const xAOD::Jet* parent(*pparent);
      m_vrc_r10rho250_jets_Reff.push_back(parent->getAttribute<float>("EffectiveR"));

      //m_vrc_r10rho250_jets_m.push_back(jet->getAttribute<float>("EffectiveR"));
      m_vrc_r10rho250_jets_nconst.push_back(jet->numConstituents());
    }

    //
    // -- R = 1.2, rho = 300 GeV
    //
    const xAOD::JetContainer* vrc_r12rho300_jets = 0;
    const xAOD::JetAuxContainer* vrc_r12rho300_jets_aux = 0;
    if(!m_store->retrieve(vrc_r12rho300_jets, vrc_r12rho300_name).isSuccess()){
      std::cout << "TreeMaker could not retrieve " << vrc_r12rho300_name << std::endl;
    }
    if(!m_store->retrieve(vrc_r12rho300_jets_aux, vrc_r12rho300_name + "Aux.").isSuccess()){
      std::cout << "TreeMaker could not retrieve " << vrc_r12rho300_name << "Aux." << std::endl;
    }
    m_vrc_r12rho300_jets_n = vrc_r12rho300_jets->size();
    for (auto jet : *vrc_r12rho300_jets) {
      m_vrc_r12rho300_jets_pt.push_back(jet->pt() / MEV);
      m_vrc_r12rho300_jets_eta.push_back(jet->eta());
      m_vrc_r12rho300_jets_phi.push_back(jet->phi());
      m_vrc_r12rho300_jets_e.push_back(jet->e() / MEV);
      m_vrc_r12rho300_jets_m.push_back(jet->m() / MEV);

      const ElementLink<xAOD::JetContainer> pparent(jet->getAttribute<ElementLink<xAOD::JetContainer> >("Parent"));
      const xAOD::Jet* parent(*pparent);
      m_vrc_r12rho300_jets_Reff.push_back(parent->getAttribute<float>("EffectiveR"));

      //m_vrc_r12rho300_jets_m.push_back(jet->getAttribute<float>("EffectiveR"));
      m_vrc_r12rho300_jets_nconst.push_back(jet->numConstituents());
    }

  }
  return;
}

//______________________________________________________________________________
//
void TreeMaker::Fill_rc_jets(xAOD::TStore*& m_store, std::string sys_name, bool is_nominal, bool affects_jets, int /*maxjets_mjsum*/)
{
  std::string rc_R10PT05_name = "RC10Jets";
  if(!is_nominal && affects_jets)
  rc_R10PT05_name += sys_name;

  const xAOD::JetContainer* rc_R10PT05_jets = 0;
  const xAOD::JetAuxContainer* rc_R10PT05_jets_aux = 0;

  if(!m_store->retrieve(rc_R10PT05_jets, rc_R10PT05_name).isSuccess()){
    std::cout << "TreeMaker could not retrieve " << rc_R10PT05_name << std::endl;
  }
  if(!m_store->retrieve(rc_R10PT05_jets_aux, rc_R10PT05_name + "Aux.").isSuccess()){
    std::cout << "TreeMaker could not retrieve " << rc_R10PT05_name << "Aux." << std::endl;
  }

  m_rc_R10PT05_jets_n = rc_R10PT05_jets->size();

  //Apply JMR if needed
  if(sys_name.find("JET_JMR")!=std::string::npos){
    for (auto jet : *rc_R10PT05_jets) {
      const xAOD::Jet* subjet;
      TLorentzVector jetSmeared; jetSmeared.Clear();
      for(auto constit: jet->getConstituents()) {
        const double smear = m_rdm->Gaus(1,0.20);
        subjet = static_cast<const xAOD::Jet*>(constit->rawConstituent());
        float smr_mass = subjet->m()*smear;
        TLorentzVector smr_subjet; smr_subjet.Clear();
        smr_subjet.SetPtEtaPhiM(subjet->pt(),subjet->eta(),subjet->phi(),smr_mass);
        jetSmeared+=smr_subjet;
      }
      m_rc_R10PT05_jets_pt.push_back(jetSmeared.Pt() / MEV);
      m_rc_R10PT05_jets_eta.push_back(jetSmeared.Eta());
      m_rc_R10PT05_jets_phi.push_back(jetSmeared.Phi());
      m_rc_R10PT05_jets_e.push_back(jetSmeared.E() / MEV);
      m_rc_R10PT05_jets_m.push_back(jetSmeared.M() / MEV);
      m_rc_R10PT05_jets_nconst.push_back(jet->numConstituents());
    }
  } else {
    for (auto jet : *rc_R10PT05_jets) {
      m_rc_R10PT05_jets_pt.push_back(jet->pt() / MEV);
      m_rc_R10PT05_jets_eta.push_back(jet->eta());
      m_rc_R10PT05_jets_phi.push_back(jet->phi());
      m_rc_R10PT05_jets_e.push_back(jet->e() / MEV);
      m_rc_R10PT05_jets_m.push_back(jet->m() / MEV);
      m_rc_R10PT05_jets_nconst.push_back(jet->numConstituents());
    }
  }
}

//______________________________________________________________________________
//
void TreeMaker::Fill_ak10_jets(const xAOD::JetContainer* v_ak10_jets)
{

  m_ak10_jets_n = v_ak10_jets->size();
  for (auto jet : *v_ak10_jets) {

    m_ak10_jets_pt.push_back(jet->pt() / MEV);
    m_ak10_jets_eta.push_back(jet->eta());
    m_ak10_jets_phi.push_back(jet->phi());
    m_ak10_jets_e.push_back(jet->e() / MEV);
    m_ak10_jets_m.push_back(jet->m() / MEV);

    float split12 = jet->getAttribute<float>("Split12") / MEV;
    float split23 = jet->getAttribute<float>("Split23") / MEV;

    m_ak10_jets_SPLIT12.push_back(split12);
    m_ak10_jets_SPLIT23.push_back(split23);

    float tau1 = jet->getAttribute<float>("Tau1_wta");
    float tau2 = jet->getAttribute<float>("Tau2_wta");
    float tau3 = jet->getAttribute<float>("Tau3_wta");

    m_ak10_jets_Tau21.push_back(tau2 / tau1);
    m_ak10_jets_Tau32.push_back(tau3 / tau2);

    // bool loose  = (jet->m() / MEV > 100.) && (split12 > 40.);
    // bool medium = loose && (split23 > 20.);
    // bool tight  = (split12 > 40.) && (tau2/tau1 > 0.4 && tau2/tau1 < 0.9) && (tau3 / tau2 < 0.65);

    // m_ak10_jets_isTopLoose.push_back(loose);
    // m_ak10_jets_isTopMedium.push_back(medium);
    // m_ak10_jets_isTopTight.push_back(tight);

    int isTopLoose = -1;
    int isTopTight = -1;
    int isTopSmoothLoose = -1;
    int isTopSmoothTight = -1;
    jet->getAttribute("LooseTopTag",isTopLoose);
    jet->getAttribute("TightTopTag",isTopTight);
    jet->getAttribute("LooseSmoothTopTag",isTopSmoothLoose);
    jet->getAttribute("TightSmoothTopTag",isTopSmoothTight);

    m_ak10_jets_isTopTight.push_back(isTopTight);
    m_ak10_jets_isTopLoose.push_back(isTopLoose);
    m_ak10_jets_isTopSmoothTight.push_back(isTopSmoothTight);
    m_ak10_jets_isTopSmoothLoose.push_back(isTopSmoothLoose);
  }
}

//_________________________________________________________________________
// jets b-tagging efficiency for the matrix method
void TreeMaker::Fill_bTag_efficiencies(const xAOD::JetContainer* v_jets){

  CP::CorrectionCode result;

  for (auto jet : *v_jets) {
    if(jet->auxdata<char>("baseline")==0) continue;

    float efficiency = 0;
    float efficiency_down = 0;
    float efficiency_up = 0;

    Int_t truth_label = -1;
    jet->getAttribute("HadronConeExclTruthLabelID", truth_label);

    if(fabs(jet->eta())<2.5) {
      result = m_bTag_tool->getEfficiency(*jet,efficiency);
      if( result!=CP::CorrectionCode::Ok) {
        std::cout << "get b-tagging efficiency failed"<<std::endl;
      }

      // systematics interface
      CP::SystematicSet systs = m_bTag_tool->affectingSystematics();
      for( CP::SystematicSet::const_iterator iter = systs.begin(); iter!=systs.end(); ++iter) {
        CP::SystematicVariation var = *iter;

        if(truth_label==5 && (var.name().find("B_systematics")==std::string::npos)) continue;
        else if((truth_label==4 || truth_label==15) && (var.name().find("C_systematics")==std::string::npos)) continue;
        else if(truth_label<4 && (var.name().find("Light_systematics")==std::string::npos)) continue;

        CP::SystematicSet set;
        set.insert(var);
        CP::SystematicCode result_syst = m_bTag_tool->applySystematicVariation(set);
        if( result_syst !=CP::SystematicCode::Ok) {
          std::cout << var.name() << " apply systematic variation failed for " <<  var.name() << std::endl;
        }
        if(var.name().find("down")!=std::string::npos) result = m_bTag_tool->getEfficiency(*jet,efficiency_down);
        else if(var.name().find("up")!=std::string::npos) result = m_bTag_tool->getEfficiency(*jet,efficiency_up);
        if( result!=CP::CorrectionCode::Ok) {
          std::cout << var.name() << " get b-tagging efficiency failed for systematics " <<  var.name() << std::endl;
        }
      }
      // switch back off the systematics
      CP::SystematicSet defaultSet;
      CP::SystematicCode reset_syst = m_bTag_tool->applySystematicVariation(defaultSet);
      if (reset_syst != CP::SystematicCode::Ok) {
        std::cout << "problem disabling systematics setting!" << std::endl;
      }
    }
    m_jets_bTagEff_85.push_back(efficiency);
    m_jets_bTagEff_85_down.push_back(efficiency_down);
    m_jets_bTagEff_85_up.push_back(efficiency_up);
  }
}


//_________________________________________________________________________
// jets b-tagging efficiency for the matrix method
void TreeMaker::Fill_matrix_method(const xAOD::JetContainer* v_jets){

  std::vector<float> v_pt;
  std::vector<float> v_eta;
  std::vector<bool>  v_isb;
  std::vector<float> v_Beff;
  std::vector<float> v_Beff_down;
  std::vector<float> v_Beff_up;

  u_int nb_jets = 0;
  float mt = 0;

  CP::CorrectionCode result;

  for (auto jet : *v_jets) {
    if(jet->auxdata<char>("baseline")==0) continue;
    if(jet->auxdata<char>("signal")==0) continue;
    if(jet->pt() / MEV < 30.0) continue;
    if(fabs(jet->eta())>2.5) continue;

    nb_jets ++;

    // create a jet to get the b-tagging efficiency
    xAOD::Jet * jet_copy = new xAOD::Jet();
    jet_copy->makePrivateStore();
    jet_copy->setJetP4(jet->jetP4());
    jet_copy->setAttribute("HadronConeExclTruthLabelID", 5);

    float efficiency = 0;
    float efficiency_down = 0;
    float efficiency_up = 0;

    result = m_bTag_tool->getEfficiency(*jet_copy,efficiency);
    if( result!=CP::CorrectionCode::Ok) {
      std::cout << "get b-tagging efficiency failed"<<std::endl;
    }

    // systematics interface
    CP::SystematicSet systs = m_bTag_tool->affectingSystematics();
    for( CP::SystematicSet::const_iterator iter = systs.begin(); iter!=systs.end(); ++iter) {
      CP::SystematicVariation var = *iter;

      if(var.name().find("B_systematics")==std::string::npos) continue;

      CP::SystematicSet set;
      set.insert(var);
      CP::SystematicCode result_syst = m_bTag_tool->applySystematicVariation(set);
      if( result_syst !=CP::SystematicCode::Ok) {
        std::cout << var.name() << " apply systematic variation failed for " <<  var.name() << std::endl;
      }
      if(var.name().find("down")!=std::string::npos) result = m_bTag_tool->getEfficiency(*jet_copy,efficiency_down);
      else if(var.name().find("up")!=std::string::npos) result = m_bTag_tool->getEfficiency(*jet_copy,efficiency_up);
      if( result!=CP::CorrectionCode::Ok) {
        std::cout << var.name() << " get b-tagging efficiency failed for systematics " <<  var.name() << std::endl;
      }
    }

    // switch back off the systematics
    CP::SystematicSet defaultSet;
    CP::SystematicCode reset_syst = m_bTag_tool->applySystematicVariation(defaultSet);
    if (reset_syst != CP::SystematicCode::Ok) {
      std::cout << "problem disabling systematics setting!" << std::endl;
    }

    v_pt.push_back(jet->pt() / MEV);
    v_eta.push_back(jet->eta());
    v_isb.push_back(jet->auxdata<char>("bjet"));
    v_Beff.push_back(efficiency);
    v_Beff_down.push_back(efficiency_down);
    v_Beff_up.push_back(efficiency_up);
  }

  // get the weights

  std::vector<double> weights_MM_30 = m_MM->GetWeight(v_pt,v_eta,v_isb,v_Beff,v_Beff_down,v_Beff_up,nb_jets, mt, 30.);
  //cout << " weights_MM[0] " <<  weights_MM[0] << endl;
  m_MM_weight_85_30        = weights_MM_30[0];
  m_MM_weight_85_30_bd     = weights_MM_30[1];
  m_MM_weight_85_30_cd     = weights_MM_30[2];
  m_MM_weight_85_30_ld     = weights_MM_30[3];
  m_MM_weight_85_30_bu     = weights_MM_30[4];
  m_MM_weight_85_30_cu     = weights_MM_30[5];
  m_MM_weight_85_30_lu     = weights_MM_30[6];
  m_MM_weight_85_30_MCstat = weights_MM_30[7];
  m_MM_weight_85_30_data   = weights_MM_30[8];

  std::vector<double> weights_MM_50 = m_MM->GetWeight(v_pt,v_eta,v_isb,v_Beff,v_Beff_down,v_Beff_up,nb_jets, mt, 50.);
  //cout << " weights_MM[0] " <<  weights_MM[0] << endl;
  m_MM_weight_85_50        = weights_MM_50[0];
  m_MM_weight_85_50_bd     = weights_MM_50[1];
  m_MM_weight_85_50_cd     = weights_MM_50[2];
  m_MM_weight_85_50_ld     = weights_MM_50[3];
  m_MM_weight_85_50_bu     = weights_MM_50[4];
  m_MM_weight_85_50_cu     = weights_MM_50[5];
  m_MM_weight_85_50_lu     = weights_MM_50[6];
  m_MM_weight_85_50_MCstat = weights_MM_50[7];
  m_MM_weight_85_50_data   = weights_MM_50[8];

  std::vector<double> weights_MM_70 = m_MM->GetWeight(v_pt,v_eta,v_isb,v_Beff,v_Beff_down,v_Beff_up,nb_jets, mt, 70.);
  //cout << " weights_MM[0] " <<  weights_MM[0] << endl;
  m_MM_weight_85_70        = weights_MM_70[0];
  m_MM_weight_85_70_bd     = weights_MM_70[1];
  m_MM_weight_85_70_cd     = weights_MM_70[2];
  m_MM_weight_85_70_ld     = weights_MM_70[3];
  m_MM_weight_85_70_bu     = weights_MM_70[4];
  m_MM_weight_85_70_cu     = weights_MM_70[5];
  m_MM_weight_85_70_lu     = weights_MM_70[6];
  m_MM_weight_85_70_MCstat = weights_MM_70[7];
  m_MM_weight_85_70_data   = weights_MM_70[8];

  std::vector<double> weights_MM_90 = m_MM->GetWeight(v_pt,v_eta,v_isb,v_Beff,v_Beff_down,v_Beff_up,nb_jets, mt, 90.);
  //cout << " weights_MM[0] " <<  weights_MM[0] << endl;
  m_MM_weight_85_90        = weights_MM_90[0];
  m_MM_weight_85_90_bd     = weights_MM_90[1];
  m_MM_weight_85_90_cd     = weights_MM_90[2];
  m_MM_weight_85_90_ld     = weights_MM_90[3];
  m_MM_weight_85_90_bu     = weights_MM_90[4];
  m_MM_weight_85_90_cu     = weights_MM_90[5];
  m_MM_weight_85_90_lu     = weights_MM_90[6];
  m_MM_weight_85_90_MCstat = weights_MM_90[7];
  m_MM_weight_85_90_data   = weights_MM_90[8];

}

//_________________________________________________________________________
//
void TreeMaker::Fill(std::string sys_name)
{
  m_tree_map[sys_name]->Fill();
}

//_________________________________________________________________________
//
void TreeMaker::Write()
{
  for(auto &it : m_tree_map) {
    it.second->Write();
  }
}

//_________________________________________________________________________
//
void TreeMaker::Reset()
{

  m_event_number = 1;
  m_run_number = -1;
  m_lumiblock_number = -1;
  m_average_interactions_per_crossing = -1;
  m_actual_interactions_per_crossing = -1;
  m_n_vtx = 0;
  m_primary_vertex_z = -1;
  m_ttbar_class = -999;
  m_ttbar_class_ext = -999;
  m_ttbar_class_prompt = -999;

  m_top_decay_type = 0;
  m_antitop_decay_type = 0;
  m_ttbar_hf_filter_flag = -1;

  m_muons_n = -1;
  m_electrons_n = -1;
  m_jets_n = -1;
  m_vrc_r10rho350_jets_n = -1;
  m_vrc_r10rho300_jets_n = -1;
  m_vrc_r12rho300_jets_n = -1;
  m_vrc_r10rho250_jets_n = -1;

  //m_tst_clean = -1;
  m_metcst = -1.;
  m_metcst_phi = -999.;
  m_mettst = -1.;
  m_mettst_phi = -999.;
  m_metsoft = -1.;
  m_metsoft_phi = -999.;

  m_meff = -1;
  m_ht = -1;
  m_met_sig = -1;
  m_mt = -1;
  m_mt_min_bmet = -1;
  m_mt_min_bmetW = -1;

  for ( std::pair < const std::string, Double_t > weight : m_weight_nom_map ) {
    weight.second = -1.;
  }
  for ( std::pair < const std::string, Double_t > weight : m_weight_sys_map ) {
    weight.second = -1.;
  }

  m_muons_pt.clear();
  m_muons_eta.clear();
  m_muons_phi.clear();
  m_muons_e.clear();
  m_muons_charge.clear();
  m_muons_passOR.clear();
  m_muons_isSignal.clear();
  m_muons_isCosmic.clear();
  m_muons_isBad.clear();
  m_muons_ptvarcone20.clear();
  m_muons_ptvarcone30.clear();
  m_muons_topoetcone20.clear();
  m_muons_d0sig.clear();
  m_muons_z0.clear();
  m_muons_isTriggerMatch.clear();

  m_electrons_pt.clear();
  m_electrons_eta.clear();
  m_electrons_phi.clear();
  m_electrons_e.clear();
  m_electrons_charge.clear();
  m_electrons_passOR.clear();
  m_electrons_isSignal.clear();
  m_electrons_passId.clear();
  m_electrons_ptvarcone20.clear();
  m_electrons_ptvarcone30.clear();
  m_electrons_topoetcone20.clear();
  m_electrons_d0sig.clear();
  m_electrons_z0.clear();
  m_electrons_isTriggerMatch.clear();

  m_jets_pt.clear();
  m_jets_eta.clear();
  m_jets_phi.clear();
  m_jets_e.clear();
  m_jets_passOR.clear();
  m_jets_isBad.clear();
  m_jets_isSignal.clear();
  m_jets_jvt.clear();
  m_jets_nTracks.clear();
  m_jets_truthLabel.clear();
  m_jets_btag_weight.clear();
  m_jets_isb_60.clear();
  m_jets_isb_70.clear();
  m_jets_isb_77.clear();
  m_jets_isb_85.clear();
  m_jets_btagEff_weight.clear();

  m_jets_bTagEff_85.clear();
  m_jets_bTagEff_85_down.clear();
  m_jets_bTagEff_85_up.clear();

  m_vrc_r10rho350_jets_pt.clear();
  m_vrc_r10rho350_jets_eta.clear();
  m_vrc_r10rho350_jets_phi.clear();
  m_vrc_r10rho350_jets_e.clear();
  m_vrc_r10rho350_jets_m.clear();
  m_vrc_r10rho350_jets_Reff.clear();
  m_vrc_r10rho350_jets_nconst.clear();

  m_vrc_r10rho250_jets_pt.clear();
  m_vrc_r10rho250_jets_eta.clear();
  m_vrc_r10rho250_jets_phi.clear();
  m_vrc_r10rho250_jets_e.clear();
  m_vrc_r10rho250_jets_m.clear();
  m_vrc_r10rho250_jets_Reff.clear();
  m_vrc_r10rho250_jets_nconst.clear();

  m_vrc_r10rho300_jets_pt.clear();
  m_vrc_r10rho300_jets_eta.clear();
  m_vrc_r10rho300_jets_phi.clear();
  m_vrc_r10rho300_jets_e.clear();
  m_vrc_r10rho300_jets_m.clear();
  m_vrc_r10rho300_jets_Reff.clear();
  m_vrc_r10rho300_jets_nconst.clear();

  m_vrc_r12rho300_jets_pt.clear();
  m_vrc_r12rho300_jets_eta.clear();
  m_vrc_r12rho300_jets_phi.clear();
  m_vrc_r12rho300_jets_e.clear();
  m_vrc_r12rho300_jets_m.clear();
  m_vrc_r12rho300_jets_Reff.clear();
  m_vrc_r12rho300_jets_nconst.clear();

  //======================================
  m_rc_R10PT05_jets_pt.clear();
  m_rc_R10PT05_jets_eta.clear();
  m_rc_R10PT05_jets_phi.clear();
  m_rc_R10PT05_jets_e.clear();
  m_rc_R10PT05_jets_m.clear();
  m_rc_R10PT05_jets_nconst.clear();

  m_ak10_jets_pt.clear();
  m_ak10_jets_eta.clear();
  m_ak10_jets_phi.clear();
  m_ak10_jets_e.clear();
  m_ak10_jets_m.clear();
  m_ak10_jets_SPLIT12.clear();
  m_ak10_jets_SPLIT23.clear();
  m_ak10_jets_Tau21.clear();
  m_ak10_jets_Tau32.clear();
  m_ak10_jets_isTopLoose.clear();
  m_ak10_jets_isTopTight.clear();
  m_ak10_jets_isTopSmoothLoose.clear();
  m_ak10_jets_isTopSmoothTight.clear();

  m_mc_pt.clear();
  m_mc_eta.clear();
  m_mc_phi.clear();
  m_mc_m.clear();
  m_mc_status.clear();
  m_mc_pdgId.clear();
  m_mc_children_index.clear();

  // chiara
  m_perm_2bex.clear();
  m_perm_3bex.clear();
  m_perm_4bin.clear();
  for( auto &wei : m_ttwei_2bex ){
    m_ttwei_2bex[wei.first] = 0.;
    m_ttwei_3bex[wei.first] = 0.;
    m_ttwei_4bin[wei.first] = 0.;
  }

  for ( std::pair < std::string, int > trigger : m_trigger ) {
    trigger.second = 0;
  }
  for ( std::pair < std::string, std::vector < int > > trigger : m_el_trigger ) {
    m_el_trigger[trigger.first].clear();
  }
  for ( std::pair < std::string, std::vector < int > > trigger : m_mu_trigger ) {
    m_mu_trigger[trigger.first].clear();
  }

  m_truth_jets_n = 0;
}
