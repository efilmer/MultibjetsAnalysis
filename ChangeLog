2016-01-04 Antoine Marzin <antoine.marzin@cern.ch
	*  Update to AnalysisSUSY,2.3.41
	* tag MultibjetsAnalysis-00-01-24

2016-01-11 Antoine Marzin <antoine.marzin@cern.ch
	* change event number from int to ULong64_t in the output

2016-01-05 Antoine Marzin <antoine.marzin@cern.ch
	* Update submit macro
	* tag MultibjetsAnalysis-00-01-23

2016-01-04 Antoine Marzin <antoine.marzin@cern.ch
	*  Update to AnalysisSUSY,2.3.39
	* tag MultibjetsAnalysis-00-01-22
	
2015-12-15 Antoine Marzin <antoine.marzin@cern.ch
	*  Update to AnalysisSUSY,2.3.38b
	* add number of truth jets in HistFitter inputs for W/Z reweighting
	
2015-12-08 Antoine Marzin <antoine.marzin@cern.ch
	* Fix jets OR in meff
	* tag MultibjetsAnalysis-00-01-21	

2015-11-21 Antoine Marzin <antoine.marzin@cern.ch
	* Fix init of AnalysisSUSY,2.3.37a
	* tag MultibjetsAnalysis-00-01-20-1

2015-11-21 Antoine Marzin <antoine.marzin@cern.ch
	* Update to AnalysisSUSY,2.3.37a
	* tag MultibjetsAnalysis-00-01-20

2015-11-30 Antoine Marzin <antoine.marzin@cern.ch
	* Fix overlap removal treatment with new SUSYTools tag
	* tag MultibjetsAnalysis-00-01-19

2015-11-21 Antoine Marzin <antoine.marzin@cern.ch
	* Update to AnalysisSUSY,2.3.36a
	* tag MultibjetsAnalysis-00-01-18

2015-11-21 Antoine Marzin <antoine.marzin@cern.ch
	* Update to AnalysisSUSY,2.3.35
	* tag MultibjetsAnalysis-00-01-17
	
2015-11-21 Antoine Marzin <antoine.marzin@cern.ch
	* Update to updated GRL with 3.317 fb-1
	* more variables in the HistFitter inputs
	
2015-11-17 Antoine Marzin <antoine.marzin@cern.ch
	* Add a few variables to HistFitter inputs
	* Add SCT error veto
	
2015-11-5 Antoine Marzin <antoine.marzin@cern.ch
	* Update to last GRL with 3.343 fb-1

2015-11-5 Loic Valery <lvalery@cern.ch>
        * Fix issues with ttbb systematics

2015-11-5 Antoine Marzin <antoine.marzin@cern.ch
	* Update to new GRL with 2.674 fb-1 
	* add cross-section uncertainty in HistFitter inputs for signal
	
2015-11-4 Antoine Marzin <antoine.marzin@cern.ch
	* Update to SUSYTools-00-06-27-08: new b-tagging recommendations
	* Update to new GRL with 1.989 fb-1 
	* tag MultibjetsAnalysis-00-01-16

2015-10-29 Antoine Marzin <antoine.marzin@cern.ch
	* veto events with bad muons before OR or cosmics after OR

2015-10-27 Antoine Marzin <antoine.marzin@cern.ch
	* Fix baseline leptons in HF inputs
	* tag MultibjetsAnalysis-00-01-15	
	
2015-10-22 Antoine Marzin <antoine.marzin@cern.ch
	* update GRL with 1.7 fb-1
	* update matrix method
	
2015-10-21 Antoine Marzin <antoine.marzin@cern.ch
	* Add cross-section for signal
	* Add b-tagging efficiency weights of individual jets in the ntuple (should already be in mini-xAODs)
	* move to SUSYTools-00-06-27-03

2015-10-19 Antoine Marzin <antoine.marzin@cern.ch
	* add selection on met_truth_filter for ttbar samples
	* update matrix method
	
2015-10-15 Antoine Marzin <antoine.marzin@cern.ch
	* add all needed variables in the HF inputs
	* tag MultibjetsAnalysis-00-01-14	
	
2015-10-13 Loic Valery <lvalery@cern.ch>
        * Adding some flexible info in Run.py options (like for lepton isolation)

2015-10-11 Loic Valery <lvalery@cern.ch>
        * Update to last GRL (1 fb-1 yeah !!!!)

2015-10-07 Loic Valery <lvalery@cern.ch>
        * Adding ttbar+bb corrections and systematics (OFF by default)

2015-10-06 Loic Valery <lvalery@cern.ch>
        * Update Metadata reading for CutBookKeeper information
        * Tag as MBJ-00-01-13

2015-10-05 Loic Valery <lvalery@cern.ch>
        * Preparing integration of ttbar+HF systematics

2015-10-02 Loic Valery <lvalery@cern.ch>
        * Update for new GRL, adding news runs in submit_data_SUSY10.py

2015-10-02 Maximilian Swiatlowski <mswiatlo@cern.ch>
	* ntuple + xaod + systematics works again-- auto-flush/auto-save on the tree fixes
	* re-enabled xaod by default
	* hooray

2015-10-01 Loic Valery <lvalery@cern.ch>
        * Apply lepton selection on pt_dependant isolation
        * Remove xAOD from Run.py ... for now

2015-10-01 Antoine Marzin <antoine.marzin@cern.ch
	* tighter baseline selection: (met > 200 && nb_jets>=4) || >= 1 lepton
	* tag MultibjetsAnalysis-00-01-12

2015-10-01 Loic Valery <lvalery@cern.ch>
        * Update for scripts on the new derivation
        * Update for top pT reweighting config file

2015-09-29 Loic Valery <lvalery@cern.ch>
        * Adding PURW config files for all backgrounds and signals (SUSYTools ones)

2015-09-27 Maximilian Swiatlowski <mswiatlo@cern.ch>
  * add GRL to svn
	* xAOD systematics enabled (merging still broken... gah)
	* re-clustering switched to official tool
	* re-clustering propagates systematics properly

2015-09-25 Antoine Marzin <antoine.marzin@cern.ch>
	* update GRL
	* move to SUSYTools-00-06-24-01

2015-09-17 Loic Valery <lvalery@cern.ch>
        * converting truth particle energy and pT in GeV to be consistent with other objects in ntuples

2015-09-10 Antoine Marzin <antoine.marzin@cern.ch
	* write ntuples and HF ouputs only events which pass the baseline selection and bad jets (to make sure all trees have the same events)
	* keep writing in the mini-xAODs all events which pass the baseline selection for at least 1 systematic
	* remove kinematic variables in the ntuples to reduce the size
	* fix meff inclusive definition
	
2015-09-09 Antoine Marzin <antoine.marzin@cern.ch
	* remove automatically events with bad jets
	* remove isbad jet flag in the ntuple
	
2015-09-08 Antoine Marzin <antoine.marzin@cern.ch
	* tag MultibjetsAnalysis-00-01-11	

2015-09-08 Loic Valery <lvalery@cern.ch>
        * reduce size of jets vector (tighter selection and less branches)
        * convert energy to GeV for MC truth particles in ntuples

2015-09-05 Loic Valery <lvalery@cern.ch>
        * reduce the size of the MC information in the ntuples/mini-xAOD (should be arround 20 at most)

2015-09-03 Loic Valery <lvalery@cern.ch>
        * reduce list of triggers
        * Allows to switch OFF ttbar+HF classification (useful for signal for instance)

2015-09-01 Antoine Marzin <antoine.marzin@cern.ch
	* fix overlap removal to set the SUSYTools flag doBjetOR and doBoostedMuonOR to true
	* update submission scripts
	* tag MultibjetsAnalysis-00-01-10
	
2015-08-31 Antoine Marzin <antoine.marzin@cern.ch
	* add matrix method in the ntuples
	* Update to SUSYTools tag 00-06-23-03 (no patch needed anymore)
	* tag MultibjetsAnalysis-00-01-09
	
2015-08-28 Antoine Marzin <antoine.marzin@cern.ch
	* start implementing the matrix method

2015-08-28 Loic Valery <lvalery@cern.ch>
        * Adding lepton trigger matching (enabled using options in Run.py, otherwise not used by default)
        * Fixing lepton trigger SF (only is using trigger matching)

2015-08-28 Antoine Marzin <antoine.marzin@cern.ch
	* start implementing the matrix method

2015-08-28 Loic Valery <lvalery@cern.ch>
        * Reducing the amount of truth information stored in the ntuples and mini-xAOD without affecting ttbar+HF classification
          (still non-optimal, but only temporary: will change soon to adapt to furture derivations)
        * Adding topdecay and antitopdecay variables to the EventInfo in output mini-xAOD

2015-08-26 Maximilian Swiatlowski <mswiatlo@cern.ch>
	* Small bug fix for GRL
	* fix readme to indicate we check out SUSYTools things now
	* removed redundant CDI files

2015-08-26 Antoine Marzin <antoine.marzin@cern.ch
	* Add 25ns GRL

2015-08-26 Antoine Marzin <antoine.marzin@cern.ch
	* Update to SUSYTools tag 00-06-23
	* apply Max's sorting fix to the selection and HistFitter ouputs
	* tag MultibjetsAnalysis-00-01-08	
	
2015-08-25 Maximilian Swiatlowski <mswiatlo@cern.ch>
	* Fixed objects to use nominal when their systematic isn't being called, thanks Antoine
	* Fixed MET to use varied objects properly

2015-08-23 Maximilian Swiatlowski <mswiatlo@cern.ch>
	* sorted outputs in ntuples (NEED TO CHECK HF OUTPUTS)
	* updated top tagging, fat jet calibrations
	* systematics working with sorting (I think)
	* b-tagging error needs attention ASAP

2015-08-18 Antoine Marzin <antoine.marzin@cern.ch
	* Update to SUSYTools tag 00-06-22
	* add the is25 option
	
2015-08-13 Antoine Marzin <antoine.marzin@cern.ch>
	* change submission scripts for faster submission
	* Update to SUSYTools tag 00-06-20
	
2015-08-12 Loic Valery <lvalery@cern.ch>
        * Adding mask variable to have (anti)top decay type (value = sum(2^(abs(pdgId of W decay))))

2015-08-07 Antoine Marzin <antoine.marzin@cern.ch>
	* Update to SUSYTools tag 00-06-19
	* add more variables in the HistFitter inputs
	* add the signal_pt_dependent_iso decoration for electrons and muons in the mini-xAODs
	
2015-08-05 Maximilian Swiatlowski <mswiatlo@cern.ch>
	* change mini-xAOD to output baseline leptons, decorated with signal and passOR
	* add more information on jets (jvt, signal) to ntuples
	* fix eta cuts/selection on jets in mini-xAOD (baseline, harmonized with ntuples)

2015-08-03 Antoine Marzin <antoine.marzin@cern.ch>
	* tag MultibjetsAnalysis-00-01-06	

2015-07-29 Louis-Guillaume Gagnon <louis.guillaume.gagnon@cern.ch>
	* Change the ptvarcone20 cuts to ptvarcone20/pt cuts (electrons/muons)
	* Change the d0sig cut to abs(d0sig) cuts (electrons-muons)

2015-07-29 Antoine Marzin <antoine.marzin@cern.ch>
	* Add met truth filter definition
	* Add weights for systematics in the ntuples and HistFitters output
	* Add passId for electrons in the ntuple
	* Update to SUSYTools tag 00-06-17

2015-07-24 Antoine Marzin <antoine.marzin@cern.ch>
	* Add the renormalisation info in the HistFitter inputs

2015-07-23 Antoine Marzin <antoine.marzin@cern.ch>
	* tag MultibjetsAnalysis-00-01-05	
	
2015-07-23 Louis-Guillaume Gagnon <louis.guillaume.gagnon@cern.ch>
	* Add patch for SUSYTools to add the isID decoration for elections
	* Add dedicated isSignal variables in the ntuples for electrons and muons
	* Update getD0Sig to use SUSYTools definitions
	
2015-07-23 Antoine Marzin <antoine.marzin@cern.ch>
	* Add missing samples to the submit scripts
	* Add temporary SUSYTools patch to get the b-tagging CDI file from RootCore

2015-07-23 Giordon Stark <gstark@cern.ch>
	* Use new top tagging tools
	* Fix a segfault with the xAODs
	* Rename top tagging variables

2015-07-22 Antoine Marzin <antoine.marzin@cern.ch>
	* Move to SUSYTools-00-06-16
	* Add patch to add loose OR in SUSYTools
	* Add possibily to add all shape systematics in the output ntuple (weights for systematics still missing)

2015-07-21 Loic Valery <lvalery@cern.ch>
	* Implement top-pT reweighting (nominal+systematics)
	* Update for GRL (includes period C5)
	* Avoid CutBookKeeper checks when running on data (based on --dataSource value)

2015-07-18 Antoine Marzin <antoine.marzin@cern.ch>
	* Fix jets systematics

2015-07-17 Giordon Stark <gstark@cern.ch>
	* This fixes the `No current directory` errors
	* This fixes a lot of missing status code checks
	* This fixes the segfaults when running with systs + large-R jets
	* This fixes a lot of missing outputs and debugged messages and cleans
	  the compiler warnings too

2015-07-16 Giordon Stark <gstark@cern.ch>
	* This adds trigger metadata to the output
	  as well as copies xTrigDecision and TrigConfKeys
	* Can use the TDT downstream of MBJ correctly, making
	  a note that MBJ does not do a trigger selection :)

2015-07-16 Loic Valery <lvalery@cern.ch>
	* Update GRL

2015-07-13 Antoine Marzin <antoine.marzin@cern.ch>
	* Fix treatment of MET systematics

2015-07-13 Antoine Marzin <antoine.marzin@cern.ch>
	* fix memory leak
	* add submit scripts for SUSY10
	* tag MultibjetsAnalysis-00-01-04

2015-07-11 Antoine Marzin <antoine.marzin@cern.ch>
	* add HistFitter input maker
	* fix treatment of systematic uncertainties
	* move to AnalysisSUSY,2.3.18b
	* tag MultibjetsAnalysis-00-01-03

2015-07-04 Antoine Marzin <antoine.marzin@cern.ch>
	* update output tree to include all baseline objects (instead of only the signal ones)

2015-06-30 Antoine Marzin <antoine.marzin@cern.ch>
	* update trigger_menu_data.txt for the 2 high lumi data runs 267638 and 267639

2015-06-22 Antoine Marzin <antoine.marzin@cern.ch>
	* Move to SUSYTools-00-06-12
	* Move back to harmonised overlap removal from SUSYTools for testing (will hack SUSYTools later)

2015-06-21 Antoine Marzin <antoine.marzin@cern.ch>
	* remove obsolete Objet classes
	* update GRL

2015-06-21 Maximilian Swiatlowski <mswiatlo@cern.ch>
	* tag : MultibjetsAnalysis-00-01-02

2015-06-20 Maximilian Swiatlowski <mswiatlo@cern.ch>
  * Fix top tag issue in xAOD
	* Fix EventInfo copy
	* New EventLoopGrid version supposedly merges on the grid better

2015-06-18 Maximilian Swiatlowski <mswiatlo@cern.ch>
  * EventInfo reorganization in output... temporary but works
	* Top Tagging works-- uses DC14 JES/JMS, and Pre-recommendations from PA
	* Fixed insiduous Deep Copy crashes on rare SUSY1 and all SUSY10 events-- thanks Attila

2015-06-16 Chiara Rizzi <chiara.rizzi@cern.ch>
	* Changing jets truth label from PartonTruthLabelID to HadronConeExclTruthLabelID

2015-06-16 Loic Valery <lvalery@cern.ch>
   * Changing all TruthParticleHelper to use xAOD objects
   * Improves ttbar+HF classification to include MPIs/FSR
   * Fix bug on TruthJets pT unit

2015-06-16 Antoine Marzin <antoine.marzin@cern.ch>
   * tag : MultibjetsAnalysis-00-01-01

2015-06-16 Antoine Marzin <antoine.marzin@cern.ch>
	*  add the MetaData to the mini-xAOD (only TriggerMenu is working now)

2015-06-12 Antoine Marzin <antoine.marzin@cern.ch>
	* update MetaData to follow the latest recommendations

2015-06-12 Maximilian Swiatlowski <mswiatlo@cern.ch>
	* Fixed MET-- was always getting TST
	* Fixed DeepCopies-- copy BaseObjects straight from xAOD now as well
	* Fixed EventInfo-- deep copy here was not working

2015-06-12 Antoine Marzin <antoine.marzin@cern.ch>
   * Reorganisation of the systematic loop in MainAnalysis.cxx
   * first r20 tag : MultibjetsAnalysis-00-01-00

2015-06-10 Maximilian Swiatlowski <mswiatlo@cern.ch>
	* Removed TruthJetObject
	* Fat jets now use xAOD objects, instead of FatJetObject
	* Using xAOD MissingET more consistently now
	* Variables work again-- previously some growing pains there
	* Substantial deep copy improvements

2015-06-10 Loic Valery <loic.valery@cern.ch>
	* Add more truth info to EventInfo for xAOD dumping

2015-06-10 Antoine Marzin <antoine.marzin@cern.ch>
   * Add patched AssociationUtils to perform the overlap removal
   * Move submission scripts to MC15 (only for available ttbar and Z samples)

2015-06-09 Maximilian Swiatlowski <mswiatlo@cern.ch>
  * Rel20 migration-- first commit to trunk
  * Removed ElectronObject usage
  * Removed MuonObject usage
  * Removed JetObject usage for normal jets
  * Memory leak noticed-- will be removed asap
  * Patched in Antoine’s 2015-06-09 code, which was in the r20 branch

2015-06-09 Antoine Marzin <antoine.marzin@cern.ch>
	* Move to the b-tagger MV2c20
	* Add some lepton isolation variables in the output ntuples
	* Add trigger menu files for data and MC
	* Add 2015 GRL

2015-06-09 Antoine Marzin <antoine.marzin@cern.ch>
	* tag MultibjetsAnalysis-00-00-10 for release 19

2015-06-08 Maximilian Swiatlowski <mswiatlo@cern.ch>
  * Continuing rel20 work-- switching to SUSYTools-00-06-09 now.

2015-06-06 Maximilian Swiatlowski <mswiatlo@cern.ch>
  * Switch to r20, first try. SUSYTools-00-06-06 for now.

2015-05-12 Antoine Marzin <antoine.marzin@cern.ch>
	* Add pileup and primary vertices in the ntuples

2015-05-08 Loic Valery <lvalery@cern.ch>
    * Add mc_m for MC truth records
    * Smart slimming (follow decay chain of tops and SUSY particles and keeps all particles)
    * Truth kinematic cuts (5 GeV and |eta| < 3)

2015-04-24 Antoine Marzin <antoine.marzin@cern.ch>
	* Add nTracks for jets in the ntuple

2015-04-24 Antoine Marzin <antoine.marzin@cern.ch>
	* Add ptvarcone20 for muons in the ntuple
	* tag MultibjetsAnalysis-00-00-09

2015-04-24 Antoine Marzin <antoine.marzin@cern.ch>
	* Add jet truth flavor in the ntuple
	* Add cosmic and bad muon in the ntuple
	* Add b-tagging with flat efficiency from BTagEfficiencyReader

2015-04-22 Antoine Marzin <antoine.marzin@cern.ch>
	* Move to SUSYTools-00-05-00-28
	* Move to non harmonised overlap removal
	* Move to IsSignalElectronExp and IsSignalMuonExp with MediumIso
	* Add isolation variables in the ntuple

2015-04-21 Francesco Rubbo <rubbo@cern.ch>
	* Change trimming to FastJet method
	* Add trimming of fat jets
	* Add n constituent variable for reclustered jets

2015-04-11 Antoine Marzin <antoine.marzin@cern.ch>
	* starting from trunk with fixed top tagging bug
	* move to SUSYTools-00-05-00-26
	* tag MultibjetsAnalysis-00-00-08

2015-04-08 Antoine Marzin <antoine.marzin@cern.ch>
	* add mtt function in TruthParticleHelper

2015-04-02 Antoine Marzin <antoine.marzin@cern.ch>
	* change b-tagging OP and add b-tagging weight in the ntuple

2015-03-25 Antoine Marzin <antoine.marzin@cern.ch>
	* tag MultibjetsAnalysis-00-00-07

2015-03-24 Antoine Marzin <antoine.marzin@cern.ch>
	* Fix TreeMaker.cxx by adding missing isSignal information for electrons and muons

2015-03-23 Trisha Farooque<trisha.farooqie.cern.ch>
	* add fat jets

2015-03-19 Antoine Marzin <antoine.marzin@cern.ch>
	* Move to SUSYTools-00-05-00-24
	* tag MultibjetsAnalysis-00-00-06

2015-02-24 Antoine Marzin <antoine.marzin@cern.ch>
	* Move to SUSYTools-00-05-00-19
	* tag MultibjetsAnalysis-00-00-05

2015-02-20 Antoine Marzin <antoine.marzin@cern.ch>
	* add MC event weights
	* add trigger decicion tool (not yet fully working)

2015-02-19 Antoine Marzin <antoine.marzin@cern.ch>
	* Move to SUSYTools-00-05-00-16
	* use MV1 as b-tagger

2015-02-11 Antoine Marzin <antoine.marzin@cern.ch>
	* add truth in output NTUP
	* add reclustered jets in output NTUP
	* Fix xAOD output crash

2015-02-05 Giordon Stark <gstark@cern.ch>
	* add reclustered and trimmed jets in jetHelper

2015-01-26 Antoine Marzin <antoine.marzin@cern.ch>
	* fix memory leak
	* tag MultibjetsAnalysis-00-00-04

2015-01-22 Antoine Marzin <antoine.marzin@cern.ch>
	* tag MultibjetsAnalysis-00-00-03

2015-01-21 Antoine Marzin <antoine.marzin@cern.ch>
	* Move to SUSYTools-00-05-00-16

2015-01-08 Antoine Marzin <antoine.marzin@cern.ch>
	* Move to SUSYTools-00-05-00-15
	* add "isSignal" flag to leptons in the ntuple

2015-01-08 Antoine Marzin <antoine.marzin@cern.ch>
	* Fix mt(met,b-jet) formula for m(met) = m(w) (thanks to Dan Tovey)
	M Root/Variables.cxx

2014-12-18 Antoine Marzin <antoine.marzin@cern.ch>
	* Remove the default value for --nevents
	M scripts/Run.py

2014-12-18 Antoine Marzin <antoine.marzin@cern.ch>
	* First tag for users: MultibjetsAnalysis-00-00-02

2014-11-02 Antoine Marzin <antoine.marzin@cern.ch>
	* First commit of a package skeleton
